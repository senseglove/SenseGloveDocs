# SenseGloveDocs
Docs for all SenseGlove Software!

## Current status 
master branch: ![badge](https://gitlab.com/senseglove/SenseGloveDocs/badges/master/pipeline.svg)

### Getting the sources
```
git clone https://gitlab.com/senseglove/SenseGloveDocs
git checkout -b my-updates
```

## How to add/update documentation
Create a new branch or fork a repo and file a merge request.
Make sure you are up-to-date with master branch, before requesting a merge.
Master is updated once in a while and deployed automatically to docs.senseglove.com.

### Local install of sphinx tool
Please visit https://docs.readthedocs.io/en/stable/intro/getting-started-with-sphinx.html
Additionaly, you'll need to install the following extensions:

```
pip install sphinx==3.3.1
pip install sphinx-rtd-theme==1.0.0
pip install sphinxembeddedvideos==1.1.0.dev20190210
pip install sphinx_tabs==3.2.0
pip install docutils==0.16
pip install breathe==4.25
```

After this, you can make changes to the source files and run

```
make html
```

Output is shown in the build/html directory. It's a static website, so no need to run a server to see your changes.

### Info how to format text or add images
The source directory contains enough examples to get started. Here is more info on formatting: 
https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html

## Automatic deployment
The master branch is automatically deployed to docs.senseglove.com (Currently https://senseglove.gitlab.io/SenseGloveDocs/). So any changes merged/pushed to master will be immediately visible. 
