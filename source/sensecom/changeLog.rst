Changelog
==================

All changes made to the SenseCom software from version 1.0.0 and onward. Sorted from oldest to newest

SenseCom v1.3.1
###############

v1.3.0 for Nova+Linux. Released for Linux only.

Changed
-------

* Switched SGConnect.so with a new v2.X release. While this release has a proper Linux integration, it is based on an older version of the v1.X code, without "Idle Connections". 

Fixed
-----

* Nova Gloves will now show up as possible BTSerial connections on Linux / Unbuntu - provided they are paired properly.
* The filestream responsible for wiriting to the sessionLog.txt now only opens/closes when a new line is written. Even if the program crashes, there should still be content inside the log.

Known Bugs
----------

* Nova / WLK: Worker thread(s) may freeze while attempting to disconnect from a BTSerial connection that is already shut down. Causes device to no longer re-connect, and the program to freeze when exiting. Letting the program crash and restarting it a second later fixes the issue, though it may occur again at a later time.
* Added a popup message when connections become idle, and when the "Retry Connections" button is pressed.
* Retuned: When a Nova Glove is paired, but is not turned on, we continue to connect to them every few seconds (in case it does turn on). This causes lag in any active Bluetooth connections.


SenseCom v1.3.0
###############

BT Lag fix and new Nova Icons

Changed
-------

* New UI icons for Nova Gloves that reflect the device's actual design.
* Back-end will now stop trying to connect to paired BTSerial devices after two failed attempts. This removes most of the the Bluetooth lag, and stabilizes the working thread cleanup. 
* Added a "Retry Connections" button which appears when we fail to connect to a paired BTSerial device. Clicking on this button will restart the connection process.
* Added a popup message when connections become idle, and when the "Retry Connections" button is pressed.

Fixed
-----

* When a Nova Glove is paired, but is not turned on, we continue to connect to them every few seconds (in case it does turn on). This causes lag in any active Bluetooth connections.
* When a Nova glove is paired, but turned off on Quest 2 / Pico Neo 3, communications back-end could freeze, resulting in no connections being made until the device is unpaired.
* Nova Gloves showing a 'low battery' icon while the device's LED was still blue. Icon should now only show if device LED is red.

Known Bugs
----------

* Nova / WLK: Worker thread(s) may freeze while attempting to disconnect from a BTSerial connection that is already shut down. Causes device to no longer re-connect, and the program to freeze when exiting. Letting the program crash and restarting it a second later fixes the issue, though it may occur again at a later time.
* Nova Gloves do not show up as possible BTSerial connections on Linux / Unbuntu, which means we cannot attempt to connect with them.
* When SenseCom crashes, so does the filestream for sessionLog.txt - resulting in an empty file.


SenseCom v1.2.x
###############

Usability & Troubleshooters update

Changed
-------

* Replaced the "Help" button introduced in v1.1 with a "Hamburger Menu", which opens a sidebar. The old "Help" screen can now be reached by clicking on the "Connections" button in this sidebar.
* Added a Settings (sub)menu accesible through the sideBar, which will house any configurable settings for SenseCom.
* Added the option to enable / disable the automatic start of a calibration sequence when a HapticGlove is connected, through the settings menu.
* Changed the UI layout to show placeholder "Left Hand" and "Right Hand" unless an actual device is connected. UI will expand down if more than two devices are connected, preferring to keep sets of left- and right handed devices.
* When a critical connection error occurs (dll missing, BT errors, no connections), a message will be displayed on the SenseCom UI to indicate what is wrong, and how to solve the issue.
* Can now access the 'right-click' menu of a device by clicking on it's name.
* Added Troubleshooting software for both the SenseGlove DK1 and Nova Glove, accesible through the "right-click" menu, and the details page. These troubleshooters allow one to test some basic functinality of the gloves, and can generate a report for facilitate remote Tech Support.


Known Bugs
----------

* Nova / WLK: Worker thread(s) may freeze while attempting to disconnect from a BTSerial connection that is already shut down. Causes device to no longer re-connect, and the program to freeze when exiting. Letting the program crash and restarting it a second later fixes the issue, though it may occur again at a later time.
* Nova Gloves do not show up as possible BTSerial connections on Linux / Unbuntu, which means we cannot attempt to connect with them.
* When a Nova Glove is paired, but is not turned on, we continue to connect to them every few seconds (in case it does turn on). This causes lag in any active Bluetooth connections. 


SenseCom v1.1.x
###############

Back-End & Calibration Update

Changed
-------

* Added a "Help" button in the top left that will take one to a debugging screen. This screen will notify if any breaking changes have occured (.dll missing, no connections, BT mixups) and show the state of any connections SenseCom is working with.
* When a "Haptic Glove" device (Nova or SG DK1) connects, we automatically start a calibration procedure. A button also appears in the main screen to (re)calibrate the gloves.
* Added the ability to see the last sent FFB / Buzz commands sent to the glove for debugging purposes.
* SenseCom now comes with an installer software, which also installs prerequisite Visual C++ packages.

Known Bugs
----------

* Nova / WLK: Worker thread(s) may freeze while attempting to disconnect from a BTSerial connection that is already shut down. Causes device to no longer re-connect, and the program to freeze when exiting. Letting the program crash and restarting it a second later fixes the issue, though it may occur again at a later time.
* Nova Gloves do not show up as possible BTSerial connections on Linux / Unbuntu, which means we cannot attempt to connect with them.
* When a Nova Glove is paired, but is not turned on, we continue to connect to them every few seconds (in case it does turn on). This causes lag in any active Bluetooth connections. 


SenseCom v1.0.x
###############

The first version out of early alpha testing. Program is able to connect to SenseGlove DK1 (wired / wireless) and Nova Gloves (wireless only).
By right-clicking on a device icon, a context menu opens with which one can view device details and send a short haptic pulse.
SenseCom creates a sessionLog.txt in MyDocuments/SenseGlove that helps us debug potential issues.

Known Bugs
----------

* Nova / WLK: Worker thread(s) may freeze while attempting to disconnect from a BTSerial connection that is already shut down. Causes device to no longer re-connect, and the program to freeze when exiting. Letting the program crash and restarting it a second later fixes the issue, though it may occur again at a later time.
* Nova Gloves do not show up as possible BTSerial connections on Linux / Unbuntu, which means we cannot attempt to connect with them. SenseGlove DK1 unaffected.