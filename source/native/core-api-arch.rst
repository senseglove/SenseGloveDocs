Core API Architecture
=====================

Overview
--------

The SenseGlove API consists of two parts: A "connection library", called SGConnect, whose main function is to detect SenseGlove devices and exchange raw data, and a "core library", called SGCore, which takes this raw data from shared memory and transforms it into useable variables. SGConnect is integrated into an executable, called SenseCom. This program provides a user interface to show the connection status of your devices, and in the future, will be used to handle calibration and firmware updates.


- The SenseGlove Core API follows an Object Oriented Programming paradigm: Each device is represented as an object from which to retrieve data and/or send commands.
- Several (data) objects are passed as arguments into (static) functions that perform kinematic calculations.
- All functions and classes written by SenseGlove developers are part of the SGCore namespace.
- Each class that represents a SenseGlove device derives from an abstract SGDevice class. This superclass contains some basic functionality related to connection and device info.
- The DeviceList class is used as the main interface to the SenseCom program. Use this class to check is the program is running and to get all SGDevices connected to the system.
- There exists a static list of SGDevices in the DeviceList class. Whenever one requests access to new devices, that's when shared memory is checked.
- Objects returned through DeviceList (or via class-specific methods, such as SenseGlove.GetSenseGloves() ) are passed by reference; meaning that a device is only parsed once and not copied. You're free to either cache this object or request it every time you need it.
- All SenseGlove Device classes inherit from an abstract SGDevice class.

SenseGlove Related
------------------
- The SGCore > SG namespace contains all classes and functions specific to the SenseGlove DK1, The SenseGlove class being the main interface for this hardware.
- The SenseGlove class has a number of static functions to access either all SenseGloves connected to the system, or the first connected SenseGlove, with an optional filter for left/right hands. Note that SenseGloves that are no longer connected to the system will not be retrieved in this way.
- The IsRight() function allows one to check if an instance of SenseGlove belongs to a left- or right hand.
- Once you have a reference to a SenseGlove object / class, you are then able to send Haptic commands though a SendHaptics function. These functions allow one to set the level of force- and vibrotactile feedback for each finger. The hardware will play the indicated effect until a new command is received: for example, when a command is sent to vibrate the thumb, the hardware will continue to vibrate the thumb until a new command is sent which turns it off.
- The GetSensorData and GetGlovePose methods all return data based on the latest sensor values. They are given the appropriate output as a parameter, and will return true if nothing went wrong. It is also possible to calculate the GlovePose based on SensorData that was retrieved earlier.
- The GetHandPose outputs an estimate of the Hand pose, and requires a HandProfile to be given as a parameter, which contains information about the hand. The Solver parameter will determine how the GlovePose and HandProfile are used to calculate the HandPose. One can retrieve a basic HandProfile using HandProfile.Default(bool) specifying if it is used for a left or right hand.
