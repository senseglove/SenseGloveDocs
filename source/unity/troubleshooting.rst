Unity Troubleshooting
=====================

Last Updated 17/04/2024 for Unity Plugin version v2.6.1.

This page covers troubleshooting tips that are relevant to the :doc:`SenseGlove Unity Plugin<overview>` only. For more general advice released to SenseGlove Hardware, go to our :doc:`Main Troubleshooting Page<../trouble-shooting>`

Android Troubleshooting
-----------------------



Meta Quest: App Crashing on Startup
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Occurs in Unity 2022 LTS version and above. No problem in 2021 LTS version.

If your app crashes on startup when using the SenseGlove Unity Plugin, there might be an issue with the App permissions.

.. code-block:: java

   java.lang.SecurityException: Need android.permission.BLUETOOTH_CONNECT permission for android.content.AttributionSource@9d529e03: AdapterService getBondedDevices
   at android.os.Parcel.createExceptionOrNull(Parcel.java:2426)
   at android.os.Parcel.createException(Parcel.java:2410)
   at android.os.Parcel.readException(Parcel.java:2393)
   at android.os.Parcel.readException(Parcel.java:2335)
   at android.bluetooth.IBluetooth$Stub$Proxy.getBondedDevices(IBluetooth.java:2947)
   at android.bluetooth.BluetoothAdapter.getBondedDevices(BluetoothAdapter.java:2491)
   at com.senseglove.sgconnect.BluetoothPorts.GetSGDevices(BluetoothPorts.java:141)
   at com.senseglove.sgconnect.DeviceScanner.UpdateConnections(DeviceScanner.java:212)
   at com.senseglove.sgconnect.DeviceScanner.RunScanner(DeviceScanner.java:147)
   at com.senseglove.sgconnect.DeviceScanner$1.run(DeviceScanner.java:54)
   at java.lang.Thread.run(Thread.java:920)


This is a `known issue <https://github.com/Adjuvo/SenseGlove-Unity/issues/35>`_ with applications built for Android Tagret API 32 and above. Unity overwrites the AndroidManifest, removing BLUETOOTH_CONNECT permissions even though these are required. Previous workaround (Brute-Forcing permissions at runtime) no longer works on Unity 2022, though it does work when building with Unity 2021 LTS version. 
 
Even `adding permissions post build <https://communityforums.atmeta.com/t5/Quest-Development/SecurityException-when-accessing-Bluetooth-API/td-p/850586>`_ no longer seems to work for later Unity versions: No Permissions popup appears when launching the app for the first time.

While we work to restore the automatic addition of the right permission(s), there are two workarounds:


Manually add permissions
$$$$$$$$$$$$$$$$$$$$$$$$

- Install your application to your Meta Quest Device
- Open your Settings Menu by navigating to the 'quick settings', then clicking on 'Settings' in the top right.
- Go to Apps > Installed Apps, and find your Application name in the list. Click on your app.
- In the following menu, you can give the App permission to access "Nearby Devices" (a.k.a. Bluetooth Connections) manually.
- Your app should now have the right permissions, and will no longer crash.


Revert to Unity 2021 LTS Version
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

If you're starting a new project, this will provide you with stable build(s), and allow you to upgrade later down the line.


Vive Tracker Troubleshooting
----------------------------

Virtual hands not on the right orientation?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a known issue with the OpenVR plugin for UnityXR: When your trackers are set to the "held in hand" role, introduced as the new default in SteamVR v1.22, the plugin returns a different rotation for all other settings. Setting your Vive Tracker Role to any other role fixes this issue.

With your tracker(s) connected and SteamVR running, right-click on any tracker icon, and select the "Manage Trackers" option.
This will open an options window. In this window, click on a second "Manage Trackers" button to open the tracker roles window.
Click on the "Tracker Role" dropdown menu, and assign any role other than "Held in Hand".
You might need to restart SteamVR for this change to be applied.



Nova2 disconnecting
----------------------------------------------------------------
I keep on loosing connection with the Nova2 when sending vibration commands?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The Bluetooth Chip on the Nova2 cannot handle a large amount of commands in a short period of time (for example, when sending a new vibration command every frame).
If a continuous vibration is required, you can use an SG_CustomWaveform with the repeatInfinte variable set to true.
Force-Feedback and Active Strap (Wrist Squeeze) commands are only sent if their values change. So updating these values every frame is not recommended.
This is a known issue that will be resolved with an update.
As a short-term alternative, it is possible to build in a 'cooldown' of at least 50 ms or 4-5 frames between commands (e.g. by looking at Unity's Time.frameCount)
