C++ API Reference
=================

API reference that also contains extensive explanation for specific classes and namespaces. Use the menu left to navigate quickly!

.. toctree::
        :maxdepth: 3

Main classes
------------

.. doxygenclass:: SGCore::BetaDevice
        :members:

.. doxygenclass:: SGCore::DeviceList
        :members:

.. doxygenclass:: SGCore::DeviceModel
        :members:

HandPose
~~~~~~~~

Overview
^^^^^^^^
The HandPose is a class containing all positions- and rotations that can be used to represent a hand in 3D space. It consists of four variables:

The *isRight* boolean variable shows that this HandPose was generated for a right hand (if true) - or left hand (if false). Its mainly used to match a HandPose to the appropriate (virtual) hand.

The *handAngles* parameter represents the articulation of the finger's joints, in radians, relative to the previous hand bone. It is a 5x3 array of Vectors (Vect3D), which represents these angles as Pronation/Supination (x), Flexion/Extension (y) and Abduction/Adduction (z) of said joint. The first index indicates the finger, from thumb (0) to pinky (4), while the second indicates the joint of said finger. It does not include the fingertip.

The *jointPositions* represent the position of the hand joints in 3D space, in millimeters, relative to the wrist (0, 0, 0). JointPositions is a 5x4 array of Vectors (Vect3D). The first index of this array indicates the finger, from thumb (0) to pinky (4), while the second indicates the joint of said finger, including the fingertips. Use these to draw a wireframe, or to calculate the distance between finger joints.

The *jointRotations* represent the 3D (quaternion) rotation relative to the wrist. Note that "relative to the wrist" does not take into account the (IMU) rotation of the glove. JointRotations is a 5x4 array of Quaternions (Quat). The first index indicates the finger, from thumb (0) to pinky (4), while the second indicates the joint of said finger, including the fingertips. By 'multiplying' the output quaternions of each joint with the quaternion rotation of the wrist, you can get each rotation in your world space.

Coordinate System
^^^^^^^^^^^^^^^^^

- The x-axis is aligned along the metacarpal bone of the middle finger.
- The y-axis points towards the thumb of the right hand, and towards the pinky of the left hand.
- The z-axis runs perpendicular to the hand palm, going up from the dorsal side of the hand.

.. note:: The pronation/supination and abduction/adduction have opposite signs (+/-) for a left- and right hand due to them being opposite sides of the body.

- Pronation / Supination, a.k.a. the twist of the finger, is the angle around the x-axis (roll)..
- Flexion / Extension of the finger is the angle around the y-axis (pitch)
- Abduction / Adduction, a.k.a. the spreading of the fingers, is the angle around the z-axis (yaw)

Mapping a HandPose to a Virtual Hand Model
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Using the SenseGlove Unity SDK**

SG_HandAnimator & SG_HandModelInfo

**Using your own animation method**

This greatly depends if you're using articulation angles or quaternion rotations (relative to the wrist, or to the world origin)


.. doxygenclass:: SGCore::HandPose
        :members:

HandProfile
~~~~~~~~~~~

A HandProfile is data class that contains the variables required for any HapticGlove to calculate a HandPose.
Since the API is built to support multiple types of hand tracking over time, each with their own calibration methods, this class is used to add more variables without the function signature changing from ``HapticGlove.GetHandPose(BasicHandModel, HandProfile, HandPose).`` The HapticGlove (sub)classes select which of the variables to use from the HandProfile.

SG_HandProfile
^^^^^^^^^^^^^^
The HandProfile differs from the SG_HandProfile in the SGCore.SG namespace: The SG_HandProfile contains the variables required for a SenseGlove to calculate one's HandPose.
The SG_HandProfile contains three variables:

- The solver parameter determines how the hand angles are calculated. At the moment, only the Interpolation solver is available.
- The interpolationSet contains the (calibrated) sensor-to-handAngle interpolation sets. This set of variables is used to calculate the HandPose when using the Interpolation solver.
- The fingerThimbleOffset is an array of size 5, which contains the offset (in mm) from the tip of the last exoskeleton linkage to the fingertip. It is sorted by finger, from Thumb [0] to Pinky [4] It is used to calculate fingertip distance, and will be used for alternate solvers in the future.

.. doxygenclass:: SGCore::HandProfile
        :members:

.. doxygenclass:: SGCore::HapticGlove
        :members:

.. doxygenclass:: SGCore::Library
        :members:

.. doxygenclass:: SGCore::SenseCom
        :members:

.. doxygenclass:: SGCore::SGDevice
        :members:

.. doxygenclass:: SGCore::SharedMem
        :members:

.. doxygenclass:: SGCore::Tracking
        :members:


Reference for Calibration
-------------------------

.. doxygennamespace:: SGCore::Calibration
        :members:


Reference for Diagnostics
-------------------------

.. doxygennamespace:: SGCore::Diagnostics
        :members:


Reference for Haptic Commands
-----------------------------

Under the hood
~~~~~~~~~~~~~~
A Haptic command sent to the SenseGlove DK1 contains the levels of force- or vibrotactile feedback of each actuator. Once set, the glove will keep the actuators at these levels until a different command is received. For example; one command might tell the SenseGlove to set the Force-Feedback of the index finger to maximum, and to vibrate the thumb at 50% intensity. Until the glove receives a new command, it will keep the force on the index finger and the vibration on the thumb active.

Because the command sent to the glove contains both force- and vibrotactile information, the SenseGlove keeps track of the last sent force-feedback and vibrotactile commands. Sending only force-feedback commands will not affect the buzz motors and vice versa. However, it is faster to send both at the same time

.. code-block:: c

        // You can do this, no problem
        SenseGlove.SendHaptics(new SGCore.Haptics.SG_BuzzCmd(0, 80, 0, 0, 0)); //writes this buzz cmd and the last sent ffb cmd to memory
        SenseGlove.SendHaptics(new SGCore.Haptics.SG_FFBCmd(100, 0, 0, 0, 0)); //writes this ffb cmd and the last buzz cmd (the one above) to memory
        // But this does the exact same, and is faster by a few operations
        SenseGlove.SendHaptics(new SGCore.Haptics.SG_FFBCmd(100, 0, 0, 0, 0), new SGCore.Haptics.SG_BuzzCmd(0, 80, 0, 0, 0)); //writes both cmds to memory.

New commands are sent to the glove every 5-10 milliseconds. At that point, the latest command in memory will be sent over to the device.

Sending Multiple Commands
~~~~~~~~~~~~~~~~~~~~~~~~~
Calling a SendCmd function on your SenseGlove instance will clear the older command and replace it with your new one. If you write two haptic commands one after another, only the last one will usually be sent to the glove. This happens because the writing operation takes much less than a milisecond. When the device checks which command to send some 5 miliseconds later, it only sees the last one sent.

.. code-block:: c
        
        // C# Example
        mySenseGlove.SendHaptics(new SGCore.Haptics.SG_BuzzCmd(0, 80, 0, 0, 0)); //1: vibrate the index finger at 80% intensity.
        mySenseGlove.SendHaptics(new SGCore.Haptics.SG_BuzzCmd(50, 0, 0, 0, 0)); //2: vibrate the thumb at 50% intensity.
        // Only the thumb will vibrate, because the first command was overridden by the second one
        //If you want both to activate at the same time, use this:
        mySenseGlove.SendHaptics(new SGCore.Haptics.SG_BuzzCmd(50, 80, 0, 0, 0)); //2: vibrate thumb at 50% intensity and the index finger at 80% intensity.

As indicated before, sending only Buzz commands will not affect your last Force-Feedback command and vice versa. You could opt to handle these separately, should you so desire.

Force-Feedback parameters
~~~~~~~~~~~~~~~~~~~~~~~~~
A Force-Feedback command consists of a single integer value between 0 and 100, which represents the voltage sent to the actuator inside the brake housing. Sending a value of 0 will set the motor voltage to 0V, while sending 100 will set the actuator voltage to maximum (24V). This actuator controls the brake force on the cables running through the exoskeleton. The SenseGlove hardware cannot pull on said cables, only apply a braking force. There is currently no control system inside the API or SenseGlove to determine when Force-Feedback should disengage again.

Please note that activating the Force-Feedback will lock the cable to a certain length: Locking the cable (magnitude = 100%) when the finger is semi-flexed will mean that this finger shouldn't be able to flex further. However, the user is still free to extend their fingers and, since the cable is now locked, it won't retract to keep up with the motion anymore. The user is free to then flex again up to the original point.

Until a proper Voltage-Force mapping is done, use the following guidelines from our CTO to help you determine what to send and when:

There isn't any force added to our cables until the PWM duty cycle reaches 60%, and the maximum force is reached when the signal reaches 100%. Using a test setup involving a digital force-sensor, we determined that a brake housing operating at 24V can pull up to 40N before slipping occurs. Once it drops below 30% again, the force on the cables will fall back to 0N.

Buzz Motor Parameters
~~~~~~~~~~~~~~~~~~~~~
The Magnitude parameter of the Buzz Motors determines the intensity of the vibration (a.k.a. the Amplitude). With the Eccentric Rotating Mass (ERM) actuators in the DK1, it is impossible to change the frequency of the vibration.

Generally the vibration motors won't start spinning until a Magnitude of 30% is given.


.. doxygennamespace:: SGCore::Haptics
        :members:

Reference for Kinematics
------------------------

.. doxygennamespace:: SGCore::Kinematics
        :members:

Reference for Nova related hardware
-----------------------------------

.. doxygennamespace:: SGCore::Nova
        :members:

Reference for SenseGlove related hardware
-----------------------------------------

.. doxygennamespace:: SGCore::SG
        :members:

Utilities reference
-------------------

.. doxygennamespace:: SGCore::Util
        :members:


