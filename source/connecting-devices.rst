Connecting Devices
==================

.. note:: If you are not a developer and are having connection issues, please contact your IT sysadmin to help you with the install of the correct software.

Connection Notes
################

Connecting to Desktop
~~~~~~~~~~~~~~~~~~~~~

On Windows or Linux, an application called :doc:`SenseCom <sensecom/overview>` is used to identify and communicate with SenseGlove devices. To install the SenseCom software, refer to its :doc:`Installation Instructions <sensecom/install-instructions>`. After connecting your device(s) to your Desktop, you can run SenseCom to check if the connection was succesful. Both wired and wireless connections are supported on Desktop. For more information, check the :doc:`Compatibility Page <compatibility>`




Connecting to Android Devices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On Android devices, such as the Oculus Quest 2, Pico Neo 3, or phone, communication with SenseGlove devices is embedded in the applications itself. Only wireless SenseGlove Devices are compatible with the Android System.


Pairing SenseGlove Nova or Wireless Kit
#######################################

The SenseGlove Nova and Wireless Kit must be paired via the Bluetooth settings of your platform.

.. tabs::

   .. group-tab:: SenseGlove Nova

       All Nova gloves will have a Bluetooth identifier "NOVA-XXXX-X" in the list of nearby devices. XXXX-X is a uniquely generated ID for each Nova. The L or R at the end indicates whether the glove is left or right-handed.

   .. group-tab:: Wireless Kit

       All Wireless kits will have a Bluetooth identifier "SGWK-XXXXXXXXXX" in the list of nearby devices. XXXXXXXXXX is a uniquely generated ID for each Wireless Kit.

.. note:: 
   Make sure the device is turned on by briefly pressing the power button until the LED light turns on.
   If you cannot find your device in the list of available devices, it is possible you're already paired to it.

.. tabs::

   .. group-tab:: Windows 10+
   
       - Go to Settings > Devices > Bluetooth & other devices > Add Bluetooth or other device > Bluetooth. 
       - Select your SenseGlove device from the list of nearby devices. Use the reference above to find the right Bluetooth name.
       - After selecting your device in this list, a pop-up window will appear, asking whether you want to pair the device. 
       - Press *connect*. Your device is now paired and trusted.
       - Launch the SenseCom application. If all went well, your device should show up in its UI. If you're using a Wireless Kit, it won't show up until a SenseGlove is connected to it.

       .. note:: 
          It is normal for Nova Gloves and Wireless Kits to show up as "Paired" but not "Connected" until they you launch SenseCom. You should not need to pair/unpair Nova Gloves in between sessions.


       A video guide is available `for Nova Gloves <https://youtu.be/okSigP6aztE?t=39>`_ and `for the Wireless Kit <https://www.youtube.com/watch?v=hK55AzQgNp4>`_.

       .. note:: 
          If you cannot find your Nova Glove in the list of possible devices, you might need to change your Bluetooth Devices Discovery setting to "Advanced" as opposed to "Basic".
          
          .. image:: images/winBT_adv.png

   .. group-tab:: Oculus Quest 2 & Pro
   
       - Open your "quick settings" menu by selecting the left-most icon in your Oculus Dashboard.
       - Open the actual settings menu by clicking on the gear icon in the top right.
       - Select the "Devices" tab, and open the "Bluetooth" tab (this should be the default screen).
       - Select the "Pair" option from the "Bluetooth Pairing" section.
       - A popup will appear. Select the "Pair new Device" option at the top.
       - Select your SenseGlove device from the list of nearby devices. Use the reference above to find the right Bluetooth name.
       - After selecting your device in this list, a pop-up window will appear, asking whether you want to pair the device. 
       - Press *connect* and wait a second. 
       - Your device is now paired and trusted, and will show up in the "Previously Connected Devices" list. This means it is paired and ready to use.

       A video guide is available `here <https://www.youtube.com/shorts/uiku3zD9J8c>`_.


   .. group-tab:: Vive Focus 3

       - Go to Settings > Connectivity > Bluetooth. Select "Pair New Device"
       - Select your SenseGlove device from the list of *Other Device*. Use the reference above to find the right Bluetooth name.
       - A pop-up will appear. Select “Confirm” to complete the pairing process.
       - The Nova Glove should now show up in your Bluetooth Menu, which means it is paired and ready to go.

       .. note:: 
          You will want to also `pair your Vive Wrist Trackers <https://business.vive.com/eu/support/wrist-tracker/category_howto/pairing-and-calibrating.html>`_ in their usual manner. When you do, please indicate that you will not be using them for hand tracking.


   .. group-tab:: Linux

       - Requires SenseCom software v1.3.1 or higher.
       - You will need to pair your Nova Glove to your Linux system, so that it is recognized as an "rfcomm" device. There are various ways to do so. One of which is listed below.
       - Launch the SenseCom application. If all went well, your device should show up in its UI after a few seconds.

       Proper Nova Glove Pairing on Linux:

       .. youtube:: Swkk_KmXwq8

       sgc.sh script:

       .. code-block::

          #!/usr/bin/env sh

          CTRL_DEVICE="YOUR_BLUETOOTH_CONTROLLER_MAC_ADDRESS"
          SG_DEVICE="YOUR_SENSEGLOVE_MAC_ADDRESS"
          SG_RFCOMM="/dev/rfcomm0"
          
          bluetoothctl pairable on
          bluetoothctl discoverable on
          bluetoothctl pair ${SG_DEVICE}
          bluetoothctl trust ${SG_DEVICE}
          bluetoothctl connect ${SG_DEVICE}
          rfcomm connect ${SG_RFCOMM} ${SG_DEVICE} 1 &

       sgd.sh script:

       .. code-block::

          #!/usr/bin/env sh
          
          SG_DEVICE="YOUR_SENSEGLOVE_MAC_ADDRESS"
          SG_RFCOMM="/dev/rfcomm0"
          
          bluetoothctl disconnect ${SG_DEVICE}
          rfcomm release ${SG_RFCOMM}


       Alternative method: Using the Blueman Manager Software

       The `Blueman Manager <https://github.com/blueman-project/blueman>`_ program provides a GUI to pair, trust, and connect to Bluetooth Devices. You'll find `installation instructions here <https://techsphinx.com/linux/install-blueman-ubuntu/>`_ and a video tutorial for Nova Gloves below. 
       However, this method is less robust than the one mentioned above, and SenseGlove cannot provide extensive support due to it being a 3rd party software.

       .. youtube:: f34ofFkx_Ow




   .. group-tab:: Pico Neo 2, 3
   
       - Open the Bluetooth menu by selecting the Bluetooth icon below your dashboard.
       - Select your SenseGlove device from the list of *Other Device*. Use the reference above to find the right Bluetooth name.
       - After selecting your device in this list, a pop-up window will appear, asking whether you want to pair the device. 
       - Press *Pair Device*. Your device is now paired and trusted.

       .. note:: 
          On the latest PUI version (v4.8.19), Pico seems to have removed the possibility to connect to any Bluetooth device. The Nova Gloves will no longer show up in the list of available devices. In older PUI versions (v4.6.X, for example), and in version 5.0 and above, this is not a problem. If you cannot find your Nova Glove in your list of Bluetooth Devices on your Pico System, please update your PUI to the latest version.

   .. group-tab:: Android Phone
   
       - Open your Bluetooth menu via Settings > Connections > Bluetooth. Or by long tapping on the Bluetooth Icon in your quick settings menu.
       - Select your SenseGlove device from the list of available devices. Use the reference above to find the right Bluetooth name.
       - After selecting your device in this list, a pop-up will appear, asking whether you want to pair the device. 
       - Press *OK*. Your device is now paired and trusted.



SenseGlove DK1
##############

The SenseGlove DK1 is connected via USB cable to a LinkBox, which is in turn connected to your PC and to the mains network. The LinkBox is there to ensure the glove can draw enough power without risk to your USB connections.

- Step 1: Connect the LinkBox to the power. Install the power cable in the LinkBox. Put the other side of the power cable in a power socket (100-240V, 50-60Hz). If connected correctly, the Blue LED on the LinkBox lights up.
- Step 2: Connect the LinkBox with a computer. Install the short USB cables with the micro USB into the LinkBox. The other side of the short USB cable should be connected to a USB port on your computer.
- Step 3: Connect the SenseGloves to the LinkBox, with the two long USB cables. The micro USB plug connects to the SenseGlove and the other side of the cable is connected to the LinkBox.
- Step 4: Launch the SenseCom program on your PC. If all went well, the SenseGlove(s) should show up on the UI.

.. youtube:: 2NJV_pKUiYE

------------

Having Trouble?
###############

Check out :doc:`our Troubleshooting section <trouble-shooting>`.