Quick Start Guide
==================

VR Headset setup
################
Follow your manufacturers instructions to connect your VR headset to your development PC. The supported devices can be found in our :doc:`compatibility` list

Connecting the Glove
####################
Nova
~~~~~~~~~~~~~~~~~~~~~~~~~
.. tabs::

   .. group-tab:: Windows 10+
       - Go to Settings > Devices > Bluetooth & other devices > Add Bluetooth or other device > Bluetooth. 
       - Select your SenseGlove device (either Nova-XXXX-X or Nova 2-XXXXX-X) from the list of nearby devices. Use the reference above to find the right Bluetooth name.
       - After selecting your device in this list, a pop-up window will appear, asking whether you want to pair the device. 
       - Press *connect*. Your device is now paired and trusted.
       - Launch the SenseCom application. If all went well, your device should show up in its UI. If you're using a Wireless Kit, it won't show up until a SenseGlove is connected to it.

       .. note:: 
          It is normal for Nova Gloves and Wireless Kits to show up as "Paired" but not "Connected" until they you launch SenseCom. You should not need to pair/unpair Nova Gloves in between sessions.


       A video guide is available `for Nova Gloves <https://youtu.be/okSigP6aztE?t=39>`_ and `for the Wireless Kit <https://www.youtube.com/watch?v=hK55AzQgNp4>`_.

       .. note:: 
          If you cannot find your Nova Glove in the list of possible devices, you might need to change your Bluetooth Devices Discovery setting to "Advanced" as opposed to "Basic".
          
          .. image:: images/winBT_adv.png


   .. group-tab:: Linux

       - Requires SenseCom software v1.3.1 or higher.
       - You will need to pair your Nova Glove to your Linux system, so that it is recognized as an "rfcomm" device. There are various ways to do so. One of which is listed below.
       - Launch the SenseCom application. If all went well, your device should show up in its UI after a few seconds.

       Proper Nova Glove Pairing on Linux:

       .. youtube:: Swkk_KmXwq8

       sgc.sh script:

       .. code-block::

          #!/usr/bin/env sh

          CTRL_DEVICE="YOUR_BLUETOOTH_CONTROLLER_MAC_ADDRESS"
          SG_DEVICE="YOUR_SENSEGLOVE_MAC_ADDRESS"
          SG_RFCOMM="/dev/rfcomm0"
          
          bluetoothctl pairable on
          bluetoothctl discoverable on
          bluetoothctl pair ${SG_DEVICE}
          bluetoothctl trust ${SG_DEVICE}
          bluetoothctl connect ${SG_DEVICE}
          rfcomm connect ${SG_RFCOMM} ${SG_DEVICE} 1 &

       sgd.sh script:

       .. code-block::

          #!/usr/bin/env sh
          
          SG_DEVICE="YOUR_SENSEGLOVE_MAC_ADDRESS"
          SG_RFCOMM="/dev/rfcomm0"
          
          bluetoothctl disconnect ${SG_DEVICE}
          rfcomm release ${SG_RFCOMM}


       Alternative method: Using the Blueman Manager Software

       The `Blueman Manager <https://github.com/blueman-project/blueman>`_ program provides a GUI to pair, trust, and connect to Bluetooth Devices. You'll find `installation instructions here <https://techsphinx.com/linux/install-blueman-ubuntu/>`_ and a video tutorial for Nova Gloves below. 
       However, this method is less robust than the one mentioned above, and SenseGlove cannot provide extensive support due to it being a 3rd party software.

       .. youtube:: f34ofFkx_Ow


Setup Sensecom
##############

.. tabs::

   .. group-tab:: Windows
       - Clone the github repository (https://github.com/Adjuvo/SenseGlove-API) or download the binaries as a .zip file. 
       - Run the SenseCom_install_X_X_X.exe inside the Win/ folder.
       - Go through the installation instructions.
       - As part of the installation, you will download a Visual C++ Package. If you are developing for C++, you likely already have this package installed. This installlation can 'fail' if you already have said package installed. In that case, dismiss the message, and continue your installation.

       Alternative Method:

       - Clone the github repository (https://github.com/Adjuvo/SenseGlove-API) or download the binaries as a .zip file. 
       - You will need a Visual C++ package to be able to use the library, which you can download or find in the SenseCom/Win folder. If you're developing with C++, you'll likely have this installed already. You will need a Visual C++ package to be able to use the library, which you can download or find in the SenseCom/Win folder. If you're developing with C++, you'll likely have this installed already.
       - Run SenseCom.exe in the SenseCom/Win folder.

   .. group-tab:: Linux

       - Clone the github repository (https://github.com/Adjuvo/SenseGlove-API) or download the binaries as a .zip file. 
       - Right click the SenseCom.x86_64 in the SenseCom/Linux folder go to the permissions tab, and select "allow running as executable".
       - Ensure that you have set the permissions for SenseCom to use your serial ports via the "sudo adduser $USER dialout" command.
       - Run SenseCom.x86_64 SenseCom/Linux

       Alternatively, you can follow along with the video below:

       .. youtube:: f34ofFkx_Ow


Calibration and Hand Tracking
#############################
If you want to track your hands in 3d space, make sure that the tracker mount has been installed and the tracker you are using is attached.

After putting on the gloves, you want to run the callibration step in Sensecom to ensure proper hand tracking.


Setup Development Project
#########################

If you are using Unity you can now follow further instructions here (:doc:`/unity/getting-started`)

If you are using Unreal Engine you can now follow further instructions here (:doc:`/ue5/ue-tutorials`)

If you are using the Robot Operating System you can now follow further instructions here (:doc:`/ros/setting-up-ros`)


Troubleshooting
###############
I can't find the gloves when searching for bluetooth devices
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   Make sure the device is charged and turned on by briefly pressing the power button until the LED light turns on.
   If you cannot find your device in the list of available devices, it is possible you're already paired to it.