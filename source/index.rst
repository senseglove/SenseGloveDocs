.. SenseGlove documentation master file, created by
   sphinx-quickstart on Thu Jun  3 14:25:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SenseGlove Docs
===============

Welcome to the official developer documentation for SenseGlove devices. The table of contents below and in the sidebar should let you easily access the documentation for your topic of interest. You can also use the search function in the top left corner to look for a specific topic.

.. note:: If you are not a developer, but still interested in SenseGlove devices, you can check out the :doc:`product-introduction` page.





.. toctree::
   :maxdepth: 1
   :caption: General
   
   quick-start
   product-introduction
   nova-glove
   nova-2
   dk1-glove
   connecting-devices
   trouble-shooting
   FAQ
   compatibility

.. toctree::
   :maxdepth: 1
   :caption: SenseCom Software

   sensecom/overview
   sensecom/install-instructions
   sensecom/settings
   sensecom/changeLog


.. toctree::
   :maxdepth: 1
   :caption: Kinematics

   kinematics/dk1-kinematics

.. toctree::
   :maxdepth: 1
   :caption: Unity3D Plugin

   unity/overview
   unity/getting-started
   unity/wristTracking
   unity/calibration
   unity/tutorials
   unity/tutorial-tasks
   unity/unity-handpose
   unity/unity-haptics
   unity/unity-snapping
   unity/unity-references
   unity/unity-picoMT
   unity/troubleshooting

.. toctree::
   :maxdepth: 1
   :caption: Unreal Engine Plugin

   ue5/ue5-index

.. toctree::
   :maxdepth: 1
   :caption: Native C++/C# API

   native/core-api-intro
   native/core-api-arch
   native/core-calibration
   native/core-nova2
   native/examples
   native/handpose-core

.. toctree::
   :maxdepth: 1
   :caption: Robot Operating System (ROS)

   ros/ros-index
   ros/ros-first-timers
   ros/setting-up-ros
   ros/general-usage

.. toctree::
   :maxdepth: 1
   :caption: 3rd Party Integrations
   
   3rdparty/interhaptics
   3rdparty/quarkXR

.. toctree::
   :maxdepth: 1
   :caption: Legacy Unreal Engine Plugin

   ue4/first-steps
   ue4/editing-blueprints
   ue4/designing-interactions
   ue4/vr-bindings
   ue4/unreal-references

.. toctree::
   :maxdepth: 1
   :caption: Hardware References

   manuals
   updating-firmware

.. toctree::
   :maxdepth: 1
   :caption: About

   about

  
Indices and search
------------------

* :ref:`genindex`
* :ref:`search`
