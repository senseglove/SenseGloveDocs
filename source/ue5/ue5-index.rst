Unreal Plugin Overview
======================

The SenseGlove Unreal Plugin has been rebuilt from the ground up to better confrom to both Unreal and C++ standards.
It is now live on the Unreal Asset Store!

.. image:: images/ue-2-gizmos.jpg


As of 19/08/2024, the documentation of the SenseGlove Unreal Engine Plugin has been moved to https://unreal.docs.senseglove.com/.
You can always find the latest release version at https://unreal.docs.senseglove.com/next/.

For the most up-to-date information and links, please always check the `Unreal Asset Store page <https://www.unrealengine.com/marketplace/en-US/product/the-senseglove-unreal-engine-plugin>`_.

.. note:: 
  Like all SenseGlove software, the Unreal Plugin communicates through devices using the :doc:`SenseCom<../sensecom/overview>` software.


Resources
~~~~~~~~~

- `Get it on the Unreal Asset Store <https://www.unrealengine.com/marketplace/en-US/product/the-senseglove-unreal-engine-plugin>`_
- `Download SenseCom <https://github.com/Adjuvo/SenseCom>`_
- `Download the Source Code <https://dev.azure.com/SenseGlove/_git/SenseGlove-Unreal>`_
