QuarkXR
=======

SenseGlove has an integration with the 5G Streaming Platform `QuarkXR <https://www.quarkxr.com/>`_, allowing one to run a high-end haptics simulation on a low-end device by offloading the computation to a Windows server.

On the Client (Standalone Headset) Side, tracking data is collected and sent to the Server-Side running a Unity Simulation. 
The simulation runs as though it was connected to the physical XR Hardware. 
After rendering, screen- and haptic data is sent back to the Client side.


Developing for QuarkXR + SenseGlove
-----------------------------------

The Client-Side for such an integration would be a custom compilation of the QuarkXR App, which is configured to connect to a Server set up for your project. 
Once connected, you operate the server as though you are running a Vive Pro or Valve Index, connected to `SteamVR <https://store.steampowered.com/app/250820/SteamVR/>`_. 
You will launch your simulation from a SteamVR menu.

Because the simulation would be running on a virtual machine, a special version of the SenseGlove Back-End is required to communicate with our hardware. 
This version exchanges data over Sockets, as opposed to Bluetooth connections, and is currently only available as a custom version of our Unity Plugin.

While this custom Unity Plugin is not readily available to download, the main difference lies inside the SenseGlove back-end - Specifically the SGCoreCs.dll library in the "Plugins" folder.
You can freely develop with the standard version of our Unity Plugin on a physical PC, and switch the .dll files once you are ready to deploy to the server-side.

.. note:: Due to limitations of the current architecture, it is only possible to send data of up to two SenseGlove Devices to one server. A QuarkXR integration is therefore limited to a Singleplayer experience.

.. list-table:: QuarkXR Integration
   :widths: 25 25
   :header-rows: 1

   * - Client-Side
     - Server-Side
   * - Android Only, up to two SenseGlove devices.
     - Windows + Unity3D Only.




