SenseCom Settings
=================

This page describes the SenseCom's Settings you can adjust to your preference, and how to access them.

Accessing the Settings
----------------------

By clicking on the hamburger icon (the three horizontal stripes) in the top left of the SenseCom UI, you'll open up a side panel with several options.

.. image:: images/SC_sideMenu.png

Click on the one named "Settings" to open the corresponding menu.

.. image:: images/SC_settings.png

To close the Settings menu, you can:

* Click on the Settings button again.
* Click on the cross in the top right of the settings menu.
* Press the Hamburger Menu to close the entire side panel.
* Press the Escape key on your keyboard.

Automatic Calibration
---------------------

When this setting is enabled, SenseCom will automatically start the calibration screen when a new Nova Glove connects.
Because the Nova's sensor values are measured relative to their starting position, this step is required each time it turns on.
However, if your simulation has a built-in calibration scene, or if you'd like control over when calibration is needed, you can disable this function here.

