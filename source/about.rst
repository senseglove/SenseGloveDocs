About
=====

This website is maintained by up to 2 overworked developers who are left with almost no time for documentation.
We apologize for any inconsistencies or lack of information. 
If you spot an error, or have a specific request for documentation, you can reach us at support@senseglove.com.