Getting started
===============

.. note::
   This page is part of the Legacy Unreal Plugin, which is no longer actively maintained.
   We recommend checking out the actively maintained version :doc:`here<../ue5/ue5-index>`.


The easiest way to get started is to the clone our simple demo scene. This will
contain all assets you will need to get started. Alternatively, you can import the plugin into
your project's plugin directory. 


.. note:: 
  The following pages cover the SenseGlove Ungine Plugin v1.0, which is no longer receiving updates as of December 4th, 2020.
  Instead, a full rework of our Unreal Engine to a version 2.0 is currently available :doc:`here<../ue5/ue5-index>`.
  If you are just starting out your Unreal Engine development, we recommend using that version instead.


Prerequisities
~~~~~~~~~~~~~~

- UE4.25 and upwards
- SenseCom (:doc:`../connecting-devices`)
- C++ Development Environment

Download demo sources
~~~~~~~~~~~~~~~~~~~~~

The demo repository uses submodules. You need the `--recursive` option to fetch the submodules automatically:

.. code-block:: bash
   
   git clone --recurse-submodules https://gitlab.com/senseglove/ue4-senseglove-demo-scene

or: 

.. code-block:: bash
   
    git clone https://gitlab.com/senseglove/ue4-senseglove-demo-scene
    cd ue4-senseglove-demo-scene
    git submodule update --init --recursive


Browse to ue4-senseglove-demo-scene folder and open the the UE4 Editor by clicking on the `.uproject` file. 

.. tip:: It is also possible to debug/run the UE4 editor through Visual Code or MVS. You can right-click on the .uproject file and generate visual studio project files.


Alternative: import plugin
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. warning:: Make sure your project is a C++ project and not a blueprint-only project. This is required to complile the imported plugin folder.  

In case you already have an existing project, you can import the SenseGlove UE4 into your plugin 
directly:

.. code-block:: bash
   
   git clone https://gitlab.com/senseglove/ue4-senseglove-plugin

Copy this repository into either the Global plugin directory or Local Plugin directory of your project. If you don't have any Plugins directory in your project, you should create a Plugins directory and put this Plugin in a subdirectory: Directory structure should be similar to: My UE4 Project > Plugins > SenseGlove-Unreal-Plugin

Make sure SGCoreCore.dll is in the 'Binaries/Win64' directory of your project.

Compiling from source
+++++++++++++++++++++

Once you have imported the Plugin you can open your project by clicking on the `.uproject` file of your project. The plugin will be compiled and will the plugin will show up in UE4 Plugins overview.

Overview Demo Files
~~~~~~~~~~~~~~~~~~~

The contect directory in the UE4 Demo scene contains the following assets:

- HandMeshes/ -- compatible .fbx imports
- Examples/ -- examples blueprints for grabbing and tracking
- Level/ -- demo levels for VR and Non-VR
- Models/ -- 3D Models in the example level

Demo Blueprints
~~~~~~~~~~~~~~~

Blueprints use the visual scripting system inside Unreal Engine 4 and allows for a fast way to start prototyping your game. Instead of having to write code line by line, you do everything visually in a UI.

Demo blueprints are available inside the *Example/* folder of the content directory. SGController is the basic blueprint with basic logic used for hand tracking. 
To access SCController blueprint:

- Open the unreal demo file.
- Click **Content** > **Examples** > **SGController**

.. toctree::
   :maxdepth: 2

.. image:: images/blueprint1.png


Check hardware setup
++++++++++++++++++++

Using the *SGController* blueprint allows you to quickly check if your glove is setup correctly.

- Make sure SenseCom (:doc:`../connecting-devices`) is running.
- Click on the *Viewport* tab. The viewport tab displays the Robo pan which is linked to the hardware (Sense gloves)
- Select SGController (Self) and click the Simulation button

.. toctree::
   :maxdepth: 2

.. image:: images/simulator1.png

.. note:: Move the glove(s), or fingers of the glove to confirm whether it is aligned with the simulation.

The SGController blueprint has default logic for tracking and physics interaction. To edit this values checkout :doc:`editing-blueprints`.

Check VR setup
++++++++++++++
Similary, you can use VRPawn to check if your VR setup is correct. VRPawn is an example blueprint setup with Bindings for an HTC Vive Controller. 
To access VRPawn blueprint:

1. Open the unreal demo file.

2. Click **Content** > **Examples** > **VRPawn**

.. toctree::
   :maxdepth: 2

.. image:: images/05_Blueprints_Examples.PNG



