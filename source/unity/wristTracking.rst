Unity Wrist Tracking
====================

SenseGlove devices have no built-in wrist tracking. So we have different ways of linking controllers and/or trackers to the SenseGlove tracking scripts within Unity.

There are two parts to tracking our gloves in VR:

#. Determining the (world) position & rotation of a tracked device.
#. Determining which offsets to use to calculate the wrist location.


Step 1 is achieved using the plugins or packages relevant for the specific tracked device. If you can get your controller or tracker to show up at the appropriate location in your Scene, you can get the SenseGlove wrist tracking up and running. 

Step 2; determining which offsets to use, is dependent on which tracked device you have, as every one of these has a different shape and therefore a different tracker mount. With the intorduction of countless tracking standards that change or obfuscate device names, it is near-impossible to automatically determine what device you have attached to your glove(s).



Plugin version v2.7.0 and above
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As of Unity Plugin version v2.7.0, you now set the wirst tracking method and offsets on a project level, via the new SenseGlove Settings Menu.

You can access the SenseGlove Settings via the dropdown menu on the top of the Unity Editor: SenseGlove > Settings.

.. image:: images/SGSettings.png

The two main settings you are looking for are the :code:`Wrist Tracking Method` and :code:`Global Wrist Tracking Offsets`.


Wrist Tracking Method
---------------------

This setting determines where the (world) position & rotation for your trackers / controllers come from. There are currently two methods available: :code:`UnityXR` and :code:`Use GameObject`. Both rely on the existence of an :code:`SG_SceneTrackingLinks` component added somewhere in the heirarchy to ensure scene-specific variables (e.g. player movement) are taken into account. We recommend attaching this component to your XRRig.


:code:`UnityXR`

Uses the Unity.XR.InputDevice class to try and find the Controller(s) and/or Trackers based on characteristics. 
Since Unity's InputDevice(s) only give you the location inside the "Play Area", 
we need to know the origin of your "XRRig" to make sure we move with the player. 
The detection of tracking devices may sometimes fail if you're using a hardware-plugin combination that SenseGlove did not take into account.


:code:`Use GameObject`

Use the world position and rotation of two specific GameObject(s) within the Scene. 
In this case, we need to know which ones represent the left and right hand, using the SG_SceneTrackingLinks. 
You are responsible for making sure these GameObjects move with your player. 
This is ideal when you're using a non-standard solution, such as Pico Motion Trackers.



Global Wrist Tracking Offsets
-----------------------------

These are the (hard-coded) offsets that will be used to translate from the tracker- or controller location to a proper wrist location.
If you are using any of SenseGlove's pre-made mounting solutions, you can select your option from the dropdown list.


.. list-table:: Wrist Offset Options
   :widths: 25 50
   :header-rows: 1

   * - Offset
     - Description
   * - Unknown
     - No offsets will be applied. Our wrist will be located exactly at the tracked object's "origin".
   * - Custom
     - Displays fields for you to hard-code position and rotation offsets to be used across the project.
       Use these as through you're created a child object underneath your original tracking GameObject.
   * - Auto Detect
     - Have SenseGlove scripts attempt to detect what device you're using*
       Does not work with OpenXR.
   * - Any other option
     - Uses the offsets for the chosen hardware mount provided by SenseGlove.
       This does not work for your custom 3D printed solutions.


.. note:: 

    *Most device manufacurers and API developers have begun to systematically obfuscate the name of their specific devices, which makes automatic detection near-impossible:
    
    - OpenXR obfuscates all device names in the XR.InputDevice class: Every HMD is now an "OpenXR HMD" instead of an "Oculus Quest 2" or "Vive Focus".
    - If you have Vive Business Streaming installed, Unity will recognize your Quest Device(s) via Oculus Link as "openxr hmd".
    
    We therefore recommend selecting a specific hardware offset instead of relying on the "Auto Detect" option, until we can find a solution that can detect the appropriate device despite these limitations.


With the correct :code:`Wrist Tracking Method` and :code:`Global Wrist Tracking Offsets` set, you should be able to build your project with the appropriate hand tracking!






Plugin version v2.6.0
~~~~~~~~~~~~~~~~~~~~~


.. list-table:: Wrist Tracking Options
   :widths: 25 50
   :header-rows: 1

   * - Method
     - Description
   * - UnityXR
     - Use UnityXR's positioning System with automatically offsets. Assign an "Origin" to move the hands with the play area.
   * - UnityXRManualOffsets
     - Use UnityXR's positioning system, but use predetermined offsets.
   * - FollowObjectAutoOffsets
     - Follow a specific transform, with automatically detected offsets.
   * - FollowObjectManualOffsets
     - Follow a specific transform, but using predetermined offsets.
   * - FollowObjectNoOffsets
     - Follow a GameObject without using any offsets. Useful to override any wrist location(s) in example scenes.
   * - MyGameObject
     - This GameObject's own Transform represents the wrist rotation & position. Useful when using parenting.


Setting up wrist tracking
-------------------------

Your main interface as of Unity Plugin v2.0+ to set up these options is the SG_HapticGlove script, which is where hand tracking from the gloves is combined with wrist tracking through 3rd party devices.

.. image:: images/wristTracking.png


Using Unity XR Plugin Management
--------------------------------

This is the easiest way to set up hand tracking, provided your project uses Unity's built-in `XR Plugin Management package <https://docs.unity3d.com/Manual/com.unity.xr.management.html>`_.
You can check its installation instructions `here <https://docs.unity.cn/Packages/com.unity.xr.management@4.2/manual/index.html>`_. 
Do note that the Unity XR Plugin Management package is available in Unity Editor 2019.1 and higher.
 
If this package is installed along with the appropriate XR Device package, the SenseGlove Plugin can automatically detect any XR devices it is compatible with.
For Oculus, this is as simple as checking the 'oculus' option in the Player Settings > XR Plugin Management.


.. image:: images/xrPluginManagement.png


For other XR devices, you'll need to either place files inside your project's Packages folder, or link to an online registry.
There's packages available for:

* `SteamVR <https://github.com/ValveSoftware/unity-xr-plugin>`_, 
* `Vive Focus / XR Elite <https://developer.vive.com/resources/vive-wave/tutorials/installing-wave-xr-plugin-unity/>`_, 
* `Pico Neo 3 <https://developer-global.pico-interactive.com/sdk>`_.


.. note::
   Unity's XR Plugin Management has different tabs for each platforms. 
   If you are developing on Windows, ensure you have the appropriate XR package selected in the windows tab. 
   If you are deploying to Android, make sure you also have the appropriate XR package selected in the Android tab.

If you have both the XR Plugin management and your device's package installed, you can use any of the following Wrist Tracking Methods on your SG_HapticGlove:

* Unity XR (Auto Offsets) (Default)
* Unity XR Manual Offsets


Assign the Origin!
##################

By "Origin" we mean the GameObject that represents your player or play area location inside your scene, usually called the "XRrig". 
When using UnityXR to track devices, we unfortunately only have access to the position and rotation inside your play area.
If your XR Rig does not have a position of [0,0,0] and rotation of [0, 0, 0], or when the play area moves through the scene, your hands will not end up in the correct location.
By assigning the Origin GameObject of your SG_HapticGlove script, the tracking will move with your play area.


Using OpenXR to track devices
#############################

When you are using OpenXR to handle your input, and not your device's appropriate XR package, you will need to take a few thing into account:

* OpenXR obscures the name of your device(s) (for example "Quest 2 HMD" becomes "OpenXR Head Tracking"), so the SenseGlove API is unable to automatically detect what XR device is being used.
  Until we have a workaround for this, you will need to manually assign which tracking offsets to use for your build(s).
* OpenXR's origin on controllers may not be the same as those on the device-specific Package, resulting on your hands ending up in the wrong location despite the correct device offsets being used.
  in this case, you will need to determine the appropriate offsets yourself - for example, by making the object a Child of your controllers.



Make it follow a tracked GameObject
-----------------------------------

Instead of relying on UnityXR to handle wrist tracking, you can also link the SG_HapticGlove to a GameObject in the scene.
This method is useful if you already have your XR Rig and controller tracking up and running. Especially useful if you are not using UnityXR Plugin Management to track your devices.
Since we assume that your controllers are set up to move with your play area, there's no need to assign an Origin like in the UnityXR wrist tracking methods.

The difference between this method and making the script a child of your tracked object is that you can dynamically change offsets: 
There's no need to have the script at the correct location on startup. It lends itself well for Automatic Offsets.

* Follow Object Auto Offsets
* Follow Object Manual Offsets
* Follow Object No Offsets [deprecated - use My GameObject instead].


Make it a child of tracked object
---------------------------------

The easiest, but least scalable solution: Using the position & rotation of the GameObject that the SG_TrackedHand is attached to.
You will need to defermine your own hand location relative to the controllers.

Alternatively, you can also use this method to manually move your hands around in editor, when you do not have access to an XR Device.


* My GameObject


Tips & Tricks
~~~~~~~~~~~~~


Meta Quest Pro
--------------

In as late as Unity Editor version 2020, a Meta Quest pro will falsely identify itself as an "Oculus Quest 2" to Unity's XR Plugin Management package, but only when building for Android.
This is either due to a bug in the Meta API, or because this Unity Editor version (and the versions before it) use an older version of their API (before a Meta Quest Pro was launched).
Either way, this will likely result in your hands ending in the wrong location.

The way to solve this is to eplicitly tell the SG_HapticGlove script that you are building for Meta Quest Pro. To do so, set your Wrist Tracking Mode to anything ending in "ManualOffsets" (UnityXR_ManualOffsets or FollowObject_ManualOffsets). Then, set your wristTrackingOffsets parameter to QuestProController.

Until Meta fixes this bug, you will unfortunately have to make a build for Quest 2 and Quest Pro separately.


Vive Focus 3 Trackers
---------------------

When using the Vive Wave SDK for UnityXR Plugin management to set up your Trackers, you will need to change your project settings:
If you have the Vive Wave SDK imported, you should find a "WaveXRSettings" menu under the "XR Plugiin Management".

* Under "Tracker", make sure "Enable Tracker" is checked. If it is not, you won't be able to use the Wrist Trackers.
* Under "Natural Hand", make sure "Natural Hand" is checked. If it is not, your app will demand you switch to controllers to start it.


.. image:: images/viveWaveSDK.png


HTC Vive Trackers
-----------------

This is a known issue with the OpenVR plugin for UnityXR: When your trackers are set to the “held in hand” role, introduced as the new default in SteamVR v1.22, the plugin returns a different rotation for all other settings. Setting your Vive Tracker Role to any other role fixes this issue.

With your tracker(s) connected and SteamVR running, right-click on any tracker icon, and select the “Manage Trackers” option. This will open an options window. In this window, click on a second “Manage Trackers” button to open the tracker roles window. Click on the “Tracker Role” dropdown menu, and assign any role other than “Held in Hand”. You might need to restart SteamVR for this change to be applied.