Updating Firmware
=================

.. note::
   Powering off your device while updating firmware or failing to follow instructions can result in your device becoming unresponsive and no longer recognized as a SenseGlove Device.
   In some cases, it can result in hardware calibration being cleared.
   I the event that this happens, please contact support@senseglove.com to see if we can resurrect your glove.


Nova 1
--------

Instructions on updating your Nova Glove firmware are under development.
If you need a firmware update ASAP; please contact support@senseglove.com.


SenseGlove Dk1
----------------


The SenseGlove DK1 firmware, along with instructions on how to update is hosted on `a public github page <https://github.com/Adjuvo/firmware-releases>`_.
We recommend not updating your Dk1 unless your device is not functioning as intended.

