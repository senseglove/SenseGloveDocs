Getting Started
===============

Updated 24/07/2023 for Unity Plugin version v2.5.1.


Prerequisites
-------------

- Unity 2019.4.30f1 and upwards
- SenseCom (:doc:`../sensecom/install-instructions`)


Installing the Unity Plugin
---------------------------

.. tabs::

        .. group-tab:: Install 
        
                - Clone the github repository at https://github.com/Adjuvo/SenseGlove-Unity
                - Import the unitypackage into your Unity project.

        .. group-tab:: Upgrade

                - Clone the github repository at https://github.com/Adjuvo/SenseGlove-Unity
                - Delete the old SenseGlove folder
                - Import the unitypackage into your Unity project.


Alternatively, the following video shows explains all the steps combined:

.. youtube:: okSigP6aztE




Setting up your (XR) Hand Tracking
----------------------------------

Because the Nova Glove does not have internal positional tracking, you'll need to import your preferred (XR) Plugin.
Essentially, if you can move a GameObject with your chosen tracker / controller, you can link it to the SenseGlove hands.

Check out our :doc:`guide on wrist tracking <wristTracking>`.



Setting up Calibration
----------------------

Your Nova glove needs to be calibrated to your user's hand.
If you're running on Desktop; SenseCom will handle calibration for you.
However, when you want to make a standalone build, especially for Android, you should check out our guide on :doc:`Calibration <calibration>`.


That's it!
----------

Go over to :doc:`tutorial-tasks` for some guides on how to start developing with the SenseGlove Unity Plugin.
Alternatively, you can check our our `Unity Example Project on GitHub <https://github.com/Adjuvo/Unity-Template>`_.
