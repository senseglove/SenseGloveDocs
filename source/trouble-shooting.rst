Troubleshooting
===============

Last Updated 17/04/2024.

Having trouble with your SenseGlove product? Here's a few simple steps you can follow to solve them yourself.

Are you looking for something specific? We have more specialized Troubleshooting tips for the following APIs:

- :doc:`SenseGlove Unity Plugin<unity/troubleshooting>`


Nova Glove Troubleshooting
--------------------------

SenseCom comes with a built-in Nova Glove troubleshooter. Once the glove is connected, right click on the blue icon to bring up a toolbar. 
Click the "Troubleshoot" button to open the Nova Troubleshooter.


Nova Glove not showing up in Windows Bluetooth menu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you cannot find your Nova Glove in the list of possible devices, you might need to change your Bluetooth Devices Discovery setting to "Advanced" as opposed to "Basic".
          
.. image:: images/winBT_adv.png



Check your Thumb sensor cable(s)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Is your digital hand not moving? Are you unable to complete the calibration process?
Calibration cannot complete unless you have moved each sensor on the Nova. 
This includes the thumb abduction sensor, which is connected to a cable that comes out of the hub just above the knuckle of your index finger.

Sometimes, it is possible for this cable to get caught underneath your "tracking mount":
The optional hardware that is used to connect a controller or Vive tracker to your Nova Glove.
If you have a tracking mount attached, make sure the thumb abduction cable is not stuck.


.. image:: images/nova-stuck-cable.png



Nova Glove shows up as 'paired', but is not 'connected' in Windows
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you're looking at the Windows Bluetooth menu after following our :doc:`connection guide <connecting-devices>`, you might notice that your Nova Gloves are paired, but do not say 'connected'.
This is expected behaviour, as the gloves won't be connected with unless SenseCom is running. This is part of the BTSerial protocol that the device uses. 

If you launch SenseCom, its state should switch to 'connected' within a few seconds, provided it is turned on.
If not, you can open the SenseCom sidebar (accessible via the button in the top left) and click on the 'Connections' button to check the state(s) of your devices.

You should not need to pair / unpair your gloves while SenseCom is running.


Windows: Nova Glove won't show up in SenseCom when using a USB Bluetooth Dongle
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You've paired your Nova Glove to Windows, and you've installed SenseCom. However, when launching the .exe, your glove is not showing up in the UI.
After a number of failed attempts, a popup will show up; "Inactive connection(s) detected. Ignoring them for now".

If this happens, please ensure your Nova Gloves are turned on (the LED at the back of the hub must be on), and click the "Retry Connections" button. 

If you still can't see your Nova Gloves; your glove and USB Bluetooth Dongle may not be compatible. 
We have noticed an incompatibility between certain USB dongles for gloves running firmware version v1.12 and earlier, which built with another version of the espressif framework.
The SenseGlove team recommends the use of an `ASUS BT400 <https://www.conrad.com/p/asus-usb-bt400-bluetooth-dongle-40-1090718>`_, or a `TPLink UB400 <https://www.tp-link.com/en/home-networking/adapter/ub400/>`_, which is what we use to test each glove before shipping.

If you are unwilling or unable to switch USB Bluetooth dongles, or when you are experiencing the same issue on an integrated chip, we recommend you look into :doc:`updating your Nova Glove firmware <updating-firmware>`.


Nova Glove keeps disconnecting
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When the Nova Glove is paired to two devices that run SenseCom or SGConnect at the same time (For example, a Desktop PC and Oculus Quest 2), a conflict can occur: 
Both devices will detect a possible Nova connection, and will keep taking over the connection from one another.

Ensure that you only have one instance of SenseCom (or Android App that uses SGConnect) running at one time.
Or, at the very least, ensure they are not paired to the same gloves.


Nova Glove tracking lags every few seconds
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If your Nova Glove's connection keeps lagging every few seconds, it is usually because SenseCom is trying to connect to another Nova that is paired, but turned off.
Upgrading your SenseCom to version v1.3 and above will solve this issue, as this version will stop the connection attempts after two failed tries.
The lag should stop after the first 10-20 seconds. 

If upgrading your SenseCom does not solve the issue, or if you were already running SenseCom v1.3+, please contact the SenseGlove support using the instructions at the bottom of this page.


Vive Tracker Troubleshooting
----------------------------

Virtual hands not on the right orientation?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is a known issue with the OpenVR plugin for UnityXR: When your trackers are set to the "held in hand" role, introduced as the new default in SteamVR v1.22, the plugin returns a different rotation for all other settings. Setting your Vive Tracker Role to any other role fixes this issue.

With your tracker(s) connected and SteamVR running, right-click on any tracker icon, and select the "Manage Trackers" option.
This will open an options window. In this window, click on a second "Manage Trackers" button to open the tracker roles window.
Click on the "Tracker Role" dropdown menu, and assign any role other than "Held in Hand".
You might need to restart SteamVR for this change to be applied.


Virtual hands not on the right location?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The tracker of your right glove may be assigned to the left hand, and vice versa. If you're running a SenseGlove demo, you can swap these using the 'S' key, or via the options menu in the top left. 

Alternatively, restarting SteamVR and truning on the tracker attached to your right hand first, followed by the tracker on your left hand, then launching your appilation again should also work.


Unity / Demo Troubleshooting
----------------------------

Can't see your hands, but you do see the controller model? Then your glove may not be properly paired and connected (yet).
Can't see your hands or a controller model? Then your wrist tracking device (Vive Tracker, Quest Controller, etc) is not turned on or properly tracked by your HMD yet. 


Still having trouble?
---------------------

You can contact SenseGlove Developers via `our contact form <https://www.senseglove.com/support/>`_, which will place it into our ticketing system.
We aim to answer your question within two working days.

If you are having trouble connecting to your haptic gloves with SenseCom, you can include a "Session Log" in your support request to help us diagnose your issue. 
On Windows, you can find this file in *Documents/SenseGlove/sessionLog.txt*. 

If you are having hardware issues and can get your glove to connect to SenseCom, please run through the device Troubleshooter and attach the toubleshooting log(s) to your support request.
You can access the troubleshooter by clicking on the device name (or right-clicking the icon) and selecting the "Troubleshoot" option.