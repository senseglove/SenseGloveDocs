ROS Overview
============

ROS - `Robot Operating System <https://www.ros.org/>`_ - is a set of libraries to help one build robot applications.

ROS comes with a number of `distributions <http://docs.ros.org/>`_. SenseGlove has a number of workspaces available for ROS 1. Not yet for ROS 2.

.. note::
   You'll need the :doc:`SenseCom Software <../sensecom/overview>` to communicate with your SenseGlove Device. In addition, your device needs to be :doc:`paired to your system <../connecting-devices>`.


SenseGlove ROS Features
-----------------------

.. note::
   There is no Nova Glove Compatibility available for the SenseGlove RoS workspaces yet!


The SenseGlove ROS Workspace can
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Interface with the SenseGlove DK1 Exoskeleton Glove via SenseCom.


The SenseGlove ROS Workspace does not
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Interface with the Nova Glove via SenseCom.

Planned Features
~~~~~~~~~~~~~~~~

- Update back-end binaries from v1.X to v2.X: C++ refactor that includes Nova and Nova 2.
- Nova 1 Interface for ROS 1 (Noetic)
- Nova 2 Interface for ROS 1 (Noetic)
- Update ROS 1 (Noetic) back-end to ROS 2 (Active distributions at that time).

*Planned features may suffer delays due to changes in availability, shifts in company priorities, or unforeseen issues with 3rd party tools and review processes. SenseGlove developers can therefore not guarantee that the promised feature(s) will be ready and released at an exact time. Please take this into account when you plan your project.*


Compatibility
-------------


SenseGlove Hardware
~~~~~~~~~~~~~~~~~~~


.. list-table:: SenseGlove Hardware <> ROS
   :widths: 25 25 25
   :header-rows: 1


   * - SenseGlove Hardware
     - ROS 1
     - ROS 2
   * - :doc:`SenseGlove DK1 <../dk1-glove>`
     - `Melodic <https://github.com/Adjuvo/senseglove_ros_ws/tree/melodic-devel>`_, `Noetic <https://github.com/Adjuvo/senseglove_ros_ws/tree/noetic-devel>`_
     - N/A
   * - :doc:`SenseGlove Nova <../nova-glove>`
     - N/A
     - N/A
   * - :doc:`SenseGlove Nova 2 <../nova-2>`
     - N/A
     - N/A



ROS Distributions
~~~~~~~~~~~~~~~~~


.. list-table:: ROS Distribution <> SenseGlove Hardware
   :widths: 25 25 25 25
   :header-rows: 1


   * - ROS Distribution
     - :doc:`SenseGlove DK1 <../dk1-glove>`
     - :doc:`SenseGlove Nova <../nova-glove>`
     - :doc:`SenseGlove Nova 2 <../nova-2>`
   * - `ROS Melodic  <https://wiki.ros.org/melodic>`_
     - Yes
     - No
     - No
   * - `ROS Noetic <https://wiki.ros.org/noetic>`_
     - Yes
     - Planned
     - Planned
   * - `ROS Humble <http://docs.ros.org/en/humble/>`_
     - Planned
     - Planned
     - Planned
   * - `ROS Iron <http://docs.ros.org/en/iron/>`_
     - No
     - No
     - No


Useful Links
------------

SenseGlove `ROS GitHub Page <https://github.com/Adjuvo/senseglove_ros_ws>`_