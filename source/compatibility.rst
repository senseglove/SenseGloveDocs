Compatibility
===============
Want to use a SenseGlove, but aren't sure if it's compatible with your preferred platform?
This page will help you find which API is right for your hardware.

The SenseGlove API does not come pre-packaged with any VR plugins or software.
Provided your desired VR device has an API available for the same programming language and platform as the SenseGlove API,
the two should be compatible. Consult your device's documentation on which API and platform it is compatible with.

However; The Nova Gloves have **no on-board sensor(s) to determine the hand location** in space. 
All devices currently rely on 3rd party tracking hardware to determine the hand location.
This means you'll need to attach a Tracker or Controller to your gloves if you want to move your hand around in VR.
A hardware mount `like this one <https://www.youtube.com/watch?v=82WPC1hN0fk>`_ is required to attach such devices to the gloves.

.. note::
   This list was last updated on 04/10/2024. Compatibility is continuously changing as more hardware devices come out, and as we develop more software. This information may not always be up to date.


HMD Compatibility
~~~~~~~~~~~~~~~~~

See if your HMD is compatible with the Nova Glove, and which Development Environments are available:


.. list-table:: HMD Compatibility List
   :widths: 25 25 25 50
   :header-rows: 1

   * - HMD
     - Nova 1
     - Nova 2
     - Development Environment*
   * - Oculus Quest 3
     - Yes
     - Yes
     - Unity3D, Unreal
   * - Oculus Quest 2
     - Yes
     - Yes
     - Unity3D, Unreal
   * - Meta Quest Pro
     - Yes
     - No
     - Unity3D, Unreal
   * - Vive Focus 3, Vive XR Elite
     - Yes, using a `Vive Wrist Tracker <https://business.vive.com/eu/product/vive-wrist-tracker/>`_.
     - Yes, using a `Vive Wrist Tracker <https://business.vive.com/eu/product/vive-wrist-tracker/>`_.
     - Unity3D, Unreal
   * - Vive Pro (1 and 2), Valve Index
     - Yes, using a `Vive Tracker <https://business.vive.com/eu/product/vive-tracker/>`_
     - Yes, using a `Vive Tracker <https://business.vive.com/eu/product/vive-tracker/>`_
     - Unity3D, Unreal
   * - Pico Neo 4
     - No
     - Yes**, using :doc:`Pico Motion Trackers <unity/unity-picoMT>`
     - Unity3D, Unreal
   * - Pico Neo 3
     - Yes**, using Pico Neo 3 Controllers
     - Yes**, using :doc:`Pico Motion Trackers <unity/unity-picoMT>`
     - Unity3D
   * - Pico Neo 2
     - Yes**, using Pico Neo 2 Controllers
     - No
     - Unity3D 


*This table assumes you are using the latest version of the Unity and Unreal Plugin.

**On PUI version (v4.8.19), Pico seems to have removed the possibility to connect to specific Bluetooth devices. Nova Gloves will no longer show up in the list of available devices. In older PUI versions (v4.6.X, for example), and in version 5.0 and above, this is not a problem. If you cannot find your Nova Glove in your list of Bluetooth Devices on your Pico System, please update your PUI to the latest version.

If your HMD is not on the list above, there is no integration with the corresponding tracking hardware readily available.
It may be possible to use a 3rd Party integration or to develop your own.



Software Compatibility
~~~~~~~~~~~~~~~~~~~~~~

SenseGlove offers API's for the following languages and platforms.

.. list-table:: Compatibility List
   :widths: 25 25 25 25 50
   :header-rows: 1

   * - 
     - SenseGlove DK1
     - Nova 1
     - Nova 2
     - Supported Platforms
   * - `Unity3D 2019.4+ <https://github.com/Adjuvo/SenseGlove-Unity>`_
     - Yes, v1.0+
     - Yes, v2.0+
     - Yes, v2.6+
     - Windows, Linux, Android
   * - `Unreal Engine Plugin 1.0 (Legacy) <https://github.com/Adjuvo/SenseGlove-Unreal-Plugin>`_
     - Yes
     - Yes
     - No
     - Windows
   * - `Unreal Engine Plugin 2.0 <https://www.unrealengine.com/marketplace/en-US/product/the-senseglove-unreal-engine-plugin>`_
     - Yes
     - Yes
     - Yes, v2.0+
     - Windows, Linux, Android
   * - `ROS 1 Noetic <https://github.com/Adjuvo/senseglove_ros_ws>`_
     - Yes
     - Yes
     - Yes
     - Linux
   * - ROS 2 Humble
     - No
     - Planned
     - Planned
     - TBA
   * - `Native C++ <https://github.com/Adjuvo/SenseGlove-API>`_
     - Yes, v1.0+
     - Yes, v1.3+
     - No
     - Windows, Linux, Android


If it's not on this list, it's safe to assume that there is no API available for the desired platform or IDE.
The SenseGlove dev team is continously working to improve and expand to different platforms.
If you're curious about which platforms might be supported soon, contact info@senseglove.com.


VR Hardware Compatibility
~~~~~~~~~~~~~~~~~~~~~~~~~
Offsets for these mounts to the glove hardware and wrist are hard coded into each SenseGlove API.
If your VR hardware is compatible with any of the tracking solutions below, 
you can use the built-in hardware offsets to accurately track your hand in 3D space.

.. list-table:: Available Hardware Mounts
   :widths: 25 25 25 25 50
   :header-rows: 1

   * - 
     - Associated Platform
     - SenseGlove DK1
     - Nova 1
     - Nova 2
   * - Vive Trackers (1.0, `2.0 <https://www.vive.com/us/accessory/vive-tracker/>`_, `3.0 <https://www.vive.com/eu/accessory/tracker3/>`_)
     - SteamVR
     - Yes
     - Yes
     - Yes
   * - `Quest 2 Controllers <https://www.oculus.com/accessories/quest-2/>`_
     - Oculus Quest 2
     - No
     - Yes
     - Yes
   * - `Quest Pro Controllers <https://www.meta.com/nl/en/quest/quest-pro/>`_
     - Meta Quest Pro
     - No
     - Yes
     - Yes
   * - `Quest 3 Controllers <https://www.meta.com/nl/en/quest/quest-3/>`_
     - Meta Quest 3
     - No
     - Yes
     - Yes
   * - `Touch Controllers <https://www.oculus.com/accessories/quest/>`_
     - Oculus Rift S
     - Yes
     - No
     - No
   * - `Pico Neo 2 6DoF Controlllers <https://www.pico-interactive.com/us/neo2.html>`_
     - Pico Neo
     - No
     - Yes
     - No
   * - `Pico Neo 3 6DoF Controlllers <https://www.picoxr.com/us/neo3.html>`_
     - Pico Neo
     - No
     - Yes
     - No
   * - `Vive Focus 3 Wrist Trackers <https://business.vive.com/eu/product/vive-wrist-tracker/>`_
     - Vive Wave
     - No
     - Yes
     - Yes
   * - `Vive Focus 3 Ultimate Trackers <https://www.vive.com/eu/accessory/vive-ultimate-tracker/>`_
     - Vive Wave
     - No
     - No
     - No


Custom Tracking Hardware
------------------------
It's possible to use a different tracking solution to the ones listed above, provided they have their own API available for your desired platform(s).
This may involve finding a way to attach said tracking hardware to the glove hardware, and determining the offsets between your device and your hand's location.

SenseGlove can provide CAD data for a generic attach point to use as a basis for a custom hardware mount.

 - SenseGlove Nova Mount Base - Left: :download:`.stl<_assets/Nova_Tracker_Base_L.STL>`
 - SenseGlove Nova Mount Base - Right: :download:`.stl<_assets/Nova_Tracker_Base_R.STL>`
 - SenseGlove Nova 2 Mount Base - Left: :download:`.stl<_assets/Nova2_Mount_Base_LEFT.STL>`
 - SenseGlove Nova 2 Mount Base - Right: :download:`.stl<_assets/Nova2_Mount_Base_RIGHT.STL>`

Please note that SenseGlove cannot be held accountable for any damages done to your tracking hardware when you use this custom solution.


Consulting
~~~~~~~~~~
For consulting services please contact info@senseglove.com

