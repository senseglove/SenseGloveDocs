Unity References
================

**SenseGlove Unity Github**

Find the latest SenseGlove Unity Plugin at https://github.com/Adjuvo/SenseGlove-Unity.


**Unity Learn**

New to Unity? Find free tutorials from the company itself at https://unity.com/learn/get-started.