Pico Motion Trackers in Unity
=============================

As of 25-11-2024, there is a mounting solution available for `Pico Motion Trackers <https://www.picoxr.com/global/products/pico-motion-tracker>`_ for the SenseGlove Nova 2.
This solution allows one to mount Motion Trackers consistently onto the glove to track the wrist movement.


.. image:: images/picoMT-Nova2.jpg


As per Pico's documentation at the time of writing, Pico Motion Trackers are compatible with the following headsets:

- Pico Neo 3
- Pico 4
- Pico 4 Ultra Devices

For the most up-to-date information, refer to `Pico's official website <https://www.picoxr.com/global/products/pico-motion-tracker>`_ .




Using Pico Motion Trackers in your Unity Project
################################################

As of writing this guide, there is no way to collect the location of a Pico Motion Tracker via the UnityEngine XR Plugin Management.
Unlike Vive (Wrist) Trackers, Pico Motion trackers do not show up in the :code:`UnityEngine.InputDevices.GetDevices(List<InputDevice> inputDevices);` list.

As such, wrist tracking using Pico Motion Trackers does NOT work 'out of the box' within the SenseGlove Unity Plugin.
You will need to import the "PICO Unity Integration SDK" package into your unity project, which is available `here <https://developer.picoxr.com/resources/#sdk>`_.


PXR_Manager and Body Tracking
#############################

Create a new XR Project using `Unity's XR System <https://docs.unity3d.com/2022.3/Documentation/Manual/xr-scene-setup.html>`_.

Add the PXR_Manager script component to a GameObject within your level, and set "Body Tracking" to true

.. image:: images/PXR_Manager.png



Move GameObjects using PXR_MotionTracking 
#########################################


Pico provides an API through which one can obtain tracker locations. You can use your own script to manage their updates, or you can use the following code provided by SenseGlove:


.. note::
   The built-in offsets used by the SenseGlove Plug-in are hard-coded, and assume you are using the PXR_MotionTracking API as provided by Pico on their Motion Tracker reference page. If you're using a different API to track your Motion Trackers, we cannot guarantee that the wrist location will be correct.


Create a new script called :code:`SG_PicoMTObjects.cs`, and code below for its content.


.. code-block:: c#
   :linenos:

   using Unity.XR.PXR;
   using UnityEngine;

   /*
    * Using reference code from Pico XR, this script updates (up to) two Pico Trackers for a left and right hand tracking, for use with SenseGlove Hand Tracking.
    * https://developer.picoxr.com/document/unity/object-tracking/
    * 
    * author: max@senseglove.com
    */

   /// <summary> A script that animates two GameObjects based on Pico's Motion Tracker API. For use in Wrist Tracking for SenseGlove. </summary>
   public class SG_PicoMTObjects : MonoBehaviour
   {
    
       //---------------------------------------------------------------------------------------------
       // Variables

       /// <summary> If this variable is assigned, we will move the TrackingDevice's relative to this one. Otherwise, we set the LocalPostion instead. </summary>
       public Transform xrOrigin;

       [Header("Left Wrist Tracking")]
       public Transform leftHandTrackingDevice;
       public MotionTrackerNum trackerIndexLeftHand = MotionTrackerNum.ONE;

       [Header("Right Wrist Tracking")]
       public Transform rightHandTrackingDevice;
       public MotionTrackerNum trackerIndexRightHand = MotionTrackerNum.TWO;

       //---------------------------------------------------------------------------------------------
      // Functions

       public void UpdateTrackerLocations()
       {
           MotionTrackerMode trackingMode = PXR_MotionTracking.GetMotionTrackerMode();
           if (trackingMode == MotionTrackerMode.MotionTracking)
           {
               MotionTrackerConnectState mtConnectStates = new MotionTrackerConnectState();
               PXR_MotionTracking.GetMotionTrackerConnectStateWithSN(ref mtConnectStates);
               if (mtConnectStates.trackerSum > 0) //there is at least 1 tracker
               {
                   UpdateTransform(leftHandTrackingDevice, trackerIndexLeftHand, xrOrigin, ref mtConnectStates);
                   UpdateTransform(rightHandTrackingDevice, trackerIndexRightHand, xrOrigin, ref mtConnectStates);
               }
           }
       }

       public static void UpdateTransform(Transform obj, MotionTrackerNum trackerNum, Transform xrOrigin, ref MotionTrackerConnectState mtConnectState)
       {
           if (trackerNum == MotionTrackerNum.NONE)
               return;
           if (obj == null)
               return;
           int trackingIndex = ToTrackingIndex(trackerNum);
           if (trackingIndex < 0 || trackingIndex >= mtConnectState.trackerSum)
               return; //In case there's less trackers or ToTrackingIndex somewhow still returns an invalid index

           string sn = mtConnectState.trackersSN[trackingIndex].value.ToString().Trim();
           if (string.IsNullOrEmpty(sn))
               return;

           MotionTrackerLocations locations = new MotionTrackerLocations();
           MotionTrackerConfidence confidence = new MotionTrackerConfidence();
           // Retrieve the location of PICO Motion Tracker
           if (PXR_MotionTracking.GetMotionTrackerLocations(mtConnectState.trackersSN[trackingIndex], ref locations, ref confidence) == 1)
               return; //if this function returns 1, it was a failure. (A bit intuitive because it's usualy 0 = false, 1 = true)

           MotionTrackerLocation location = locations.localLocation; //This is relative to the Headset, but not to the player...
           Vector3 localPosition = location.pose.Position.ToVector3();
           Quaternion localRotation = location.pose.Orientation.ToQuat();
           if (xrOrigin == null)
           {
               obj.localRotation = localRotation;
               obj.localPosition = localPosition;
           }
           else
           {
               obj.localRotation = xrOrigin.rotation * localRotation;
               obj.localPosition = xrOrigin.position + (xrOrigin.rotation * localPosition);
           }
       }


       public static int ToTrackingIndex(MotionTrackerNum num)
       {
           return ((int)num) - 1;
       }


       //---------------------------------------------------------------------------------------------
       // Monobehaviour


       private void Start()
       {
           if (trackerIndexLeftHand != MotionTrackerNum.NONE)
               PXR_MotionTracking.CheckMotionTrackerModeAndNumber(MotionTrackerMode.MotionTracking, trackerIndexLeftHand);
           if (trackerIndexRightHand != MotionTrackerNum.NONE)
               PXR_MotionTracking.CheckMotionTrackerModeAndNumber(MotionTrackerMode.MotionTracking, trackerIndexRightHand);
       }

       private void Update()
       {
           UpdateTrackerLocations();
       }
   }



You will need to assign two individual GameObjects as the leftHandTrackingDevice and rightHandTrackingDevice via the Inspector.
These will represent the Pico Motion Trackers assigned to the left hand and right hand.

The "trackerIndex" variables control which of Pico's trackers to access for the corresponding hand. Unfortunately, the Pico API does not allow you to assign a left and right hand to a tracker.
The reccomended indices are "ONE" and "TWO". If you accidentally assign the wrong tracker index, it is easy to disconnect and swap the hardware on your glove.

The PXR_MotionTracking API gives us the tracker location in the "play area". You should assign this script an xrOrigin so the GameObjects move with your XR Rig. Alternatively, you can make sure your GameObject(s) are children of the XRRig instead.


.. image:: images/PicoMTObjects_inspector.png




Link GameObjects to the SenseGlove Plugin
#########################################

Now that you can move two GameObjects using the Pico Motion Trackers, we need to set up the SenseGlove Wrist Tracking recognize them.

Add a :code:`SG_SceneTrackingLinks` component anywhere in your scene. Assign the two objects you're as the appropriate leftHandTrackingDevice and rightHandTrackingDevice via the inspector.
Next, open the SenseGlove Setting Menu using the toolbar at the top "SenseGlove > Settings". Make sure the :code:`Wrist Trackng Method` is set to :code:`Use Game Object` and that the :code:`Global Wrist Tracking Offsets` are set to :code:`Pico Motion Tracker`


.. image:: images/picoMT-Settings.png


You shoud now be able to move your SenseGlove hands around usin the Pico Motion Trackers!



References
----------

- https://www.picoxr.com/global/products/pico-motion-tracker
- https://developer.picoxr.com/document/unity/object-tracking/
- https://developer.picoxr.com/resources/#sdk 
 
 