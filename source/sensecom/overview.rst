SenseCom Overview
=================
Updated at 06/10/22 for SenseCom v1.3.0

SenseCom ("Short for SenseGlove Communications") is a program that runs in the background of your operating system.
Its main purpose is to find and connect to the SenseGlove devices on your system and exchange data with them, like a "SteamVR for Haptic Gloves".
Most SenseGlove API's are configured to automatically start the software if it is not (yet) running.
It serves as the main source of information of the state of your connections.

.. note:: SenseCom is only required when communicating with Windows or Linux. For (standalone) Android devices, the communications side will be embedded into your application.


At a glance
~~~~~~~~~~~

.. image:: ../images/sensecom.png

SenseCom shows the status of your gloves, as well as any actions required using the icons on the main screen.

- An outline means your glove is not (yet) detected.
- A solid blue icon means your glove is connected and ready to go!
- A red battery icon means your wireless device is running low on battery.
- A blue battery icon with a lightning bolt means your wireless device is currently charging.
- A blue circle with an exclamation mark indicates that a firmware update is available for your device.


Calibration
~~~~~~~~~~~

Calibration is done in a separate screen, which can be accessed by pressing the 'Calibrate' button. 
This button will appear as soon as any Haptic Glove (SenseGlove DK1 or SenseGlove Nova) connects.

.. note::

    The Calibration screen is automatically opened when your Nova Glove(s) connect. If you'd like to stop that from happening, you can adjust it in :doc:`the settings <settings>`.

.. image:: images/SC_calibration.png

To calibrate your glove, you'll need to open- and close your hands until the virtual hand begins to move, then confirm your calibration with a thumbs up.
Once the calibration completes, you'll be back at the main menu, and ready to get started!


The Context Menu
~~~~~~~~~~~~~~~~

You can right click on the icon of any connected device to open up a "context menu". Alternatively, you can click on the device's name below the icon to open it.

.. image:: images/SC_contextMenu.png

The Context Menu allows one to access a "Details" window, which will provide information on battery state, firmware versions and other hardware information.
You can also use this menu to identify the device by having it give a short haptic pulse. 
Depending on which device is connected, the context menu can have additional options, such as a link to a troubleshooting page.

.. image:: images/SC_detailsMenu.png

The Hamburger Menu
~~~~~~~~~~~~~~~~~~

By clicking on the hamburger icon (the three horizontal stripes) in the top left of the SenseCom UI, you'll open up a side panel with several options.

.. image:: images/SC_sideMenu.png

* "Settings" will allow you to change several options in the SenseCom :doc:`Settings  <settings>`.
* "Connections" will bring you to a screen that shows all active connections and their current status. Use this when you can't seem to connect to your glove(s).
* "Documentation" will bring up this website.

Troubleshooters
~~~~~~~~~~~~~~~

Both the SenseGlove Nova and SenseGlove DK1 have a 'Troubleshooter' built into SenseCom. You can access these via the Context Menu.
These troubleshooters will provide you with several tools to check the functionality of your glove.

.. image:: images/SC_troubleshooter.png


Installing SenseCom
~~~~~~~~~~~~~~~~~~~

You can learn how to install SenseCom for your platform by following the :doc:`installation instructions <install-instructions>`.


Under the hood
~~~~~~~~~~~~~~

SenseCom is technically a UI around the SGConnect library, which handles the detection of- and communication with SenseGlove devices. 
This UI adds features such as calibration and firmware checks. Sense Glove recommends using SenseCom as the method of establishing communications. 
If, for any reason, you are unable or unwilling to run SenseCom in the background, it is possible to isolate the SGConnect library 
and import it into your own C++ project. 

More information can be found here: https://github.com/Adjuvo/SenseGlove-API/tree/master/SenseCom


ChangeLog
~~~~~~~~~

Can be found :doc:`here  <changeLog>`.


Support
~~~~~~~

SenseCom is now hosted on `its own GitHub Page <https://github.com/Adjuvo/SenseCom>`_.
Here, you can find the latest version(s) of the software and post any issues you might have with it.