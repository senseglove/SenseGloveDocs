SenseGlove Nova 1
===================

An evolution on the :doc:`SenseGlove DK1 exoskeleton<dk1-glove>` - designed to be fully wireless, easy to use, and compatible with standalone headsets.


.. youtube:: qpmWz6lu_ak



The Nova Glove 1.0 was released in Q3 2021, and is currently available through the `SenseGlove webshop <https://www.senseglove.com/product/nova/>`_.



Product Features
----------------

The device consists of two separate parts; the "Hub" and "Soft Glove", which can be separated.
The Hub contains all sensors and actuators, while the Soft Glove is the wearable part that ensures a good fit to the body.


Force-Feedback
~~~~~~~~~~~~~~

The Nova Glove provides 1 Degree of Freedom of Force-Feedback on the thumb, index finger, middle finger and ring finger. The glove does not provide force-feedback on the pinky finger.
The system works by applying a brake force to the cables that run along the back of the hand, which inhibits the flexion movement.
You are always able to extend your fingers, but you won't be able to flex them past a certain point.
The Nova's Force-Feedback actuators do not pull on their cables; meaning that they cannot move the fingers for you, nor can the glove move by itself.

The Nova Glove can provide up to 20N of force along each finger that is equipped with a force-feedback motor. 
From an API level, one can set the force on each of these fingers with a single command, at a rate of up to 200Hz and a resolution of 100 steps.


Vibrotactile Feedback
~~~~~~~~~~~~~~~~~~~~~

The Nova Glove has one Voice Coil Actuator (VCA) vibrotactile actuator integrated into the hub, which can create vibrations along the back of the hand.
This vibromotor is nicknamed 'the Thumper'.

In addition, the glove has a Linear Resonant Actuator (LRA) located on the tip of the thumb and index finger.
These vibration motors are connected to the Hub via conductive yarn inside the soft glove.

From an API level, one can directly set the amplitude of each of these vibration motors with a single command, at a rate of up to 200Hz and a resolution of 100 steps.



Finger Tracking
~~~~~~~~~~~~~~~

The Nova Glove measures the flexion / extension of the thumb, index finger, middle finger and ringer finger with a single sensor each, and has one additional sensor to measure thumb abduction / adduction. The pinky finger flexion is linked to that of the ring finger. These movements are captured by measuring the extension of the cables on the Nova Glove. 

The Nova Glove measures cable extensions at a rate of 60 Hz, with a resolution of 0.03 mm. However, your calibration will ultimately determine the resolution of the hand pose calculated from this data.

The sensors used to measure this extension are not absolute: When restarting the glove, the sensor readings might differ. The cable extensions will also differ between users. It is therefore required to recalibrate the Nova Glove at the beginning of each session. The SenseCom software offers a near-automatic process to handle this calibration process on a Desktop application. The Unity Plugin offers a "Calibration Void" that can be included at the start of standalone demos.



Wrist Tracking
~~~~~~~~~~~~~~

The Nova Glove does not have built in hand-tracking: It cannot determine its position in 3D space by itself, and instead relies on mouting a 3rd party device, such as a Vive Tracker, onto the glove.
Computer Vision models are being developed to track the Nova Glove(s) in 3D space using camera systems, but these are still under heavy development.


.. image:: images/nova-trackermounts.png


.. note:: 
    Integrating the glove directly into a 3rd party system, such as the `Vive Lighthouse system <https://www.roadtovr.com/tundra-labs-steamvr-tracking-hdk-tl448k6d-gp-hdk/>`_, would increase the size of the device, and limits it to that system only. Developing a tracking system built into the glove is not possible with the Nova Glove's current hardware.

The Nova Glove does contain an Inertial Measurement Unit (IMU). It uses a Magnetometer, Accelerometer and Gyroscope to measure the glove's rotation relative to the earth's magnetic field.
You can combine the IMU with a 3rd party tracking device to include lower arm tracking into your simulation, though note that an additional calibration step will be required to map the movements of the IMU to the wrist.

.. note::
   The IMU takes a few seconds to calibrate - at which point, it might 'snap' to a new rotation. This only happens once, usually in the first few seconds of turning on the glove. 


Bluetooth
~~~~~~~~~

The Nova Gloves uses the Bluetooth 4.2 protocol. Specifically the Bluetooth 4.2 BR/EDR, with SPP (serial port profile).


Latency
~~~~~~~

Determining a 'full round-trip' latency - the time between moving one's finger into a virtual object to actually feeling it - cannot be defined without knowing the full system in place.
Instead, we can provide you with two values of 'latency' for the moment the Nova Glove's data enters and leaves a program running on your device, be it a PC or a standalone headset.

This does not take into account the update rate of one's physics and/or rendering engine, which also has a significant impact on latency; adding 20 ms when running at 50 Hz (the default physics timestep for Unity) down to 11 ms when running at 90Hz (the default refresh rate of a Valve Index). 

The communication between PC and Glove is estimated to be between 10 to 12.5 ms, based on the experiments done by `M.J. Moro n, R. Luque, E. Casilari and A. Díaz-Estrella <https://www.researchgate.net/publication/3404819_Minimum_delay_bound_in_Bluetooth_transmissions_with_serial_port_profile>`_. 

These latency numbers are based on estimations, and do not take into account factors such as cable slack (which is a function of cable extension rather than time).

Incoming Sensor Data
####################

The latency between moving one's finger and having the new position registered in one's simulation will take between 10 to 29 ms:

* The Nova Glove sends its sensor data at a rate of 60 Hz, which can add up to 16.67 ms.
* Sending this data to another device will take between 10-12.5 ms.
* On the device, sensor data is posted to the SenseGlove API as soon as it is received in full, introducing negligible delays.
* Accessing this sensor data and converting it into a hand pose takes < 1 ms.


Sending Commands to the Glove
#############################

The latency between calling a function to send a new command (e.g. Setting a Force-Feedback level from 0% to 100%) and it activating on the glove will take between 15 to 22.5 ms:

* Encoding the command and making it available for the back end is negligible.
* The maxmimum send rate for the SenseGlove API is limited to 200Hz, introducing up to 5ms of delay.
* Sending a command to a Nova Glove will take between 10-12.5 ms.
* The commands are handled on the device on an interrupt basis, and have little to no delay in being processed.
* Activating the brakes from 0% of force-feedback has a mechanical delay of up to 5ms.



Battery
~~~~~~~

The Nova Glove's battery is located at the back of the Hub, behind the LED light.

A fully charged battery will last for 3-4 hours of active use. This number is based on the glove being constantly used for demonstrations during fairs. When developing with the Nova Glove, the battery will generally last longer.

When fully depleted, the battery can take between 1-2 hours to fully charge, using the USB charging port at the side of the glove, near the power buttun.
It is possible to use the Nova Glove while charging the battery.

.. note::
   The Nova Glove's charging port often does not fit larger USB micro cables. Be sure to use the USB-Micro charging cables that come with the gloves.

Shipping your Nova by plane? The battery's Material Safety DataSheet (MSDS) can be :download:`downloaded here. <pdfs/MSDS-35E.pdf>`



LED Lights
~~~~~~~~~~

On the backside of the Nova, an LED indicator communicates the status of the device. The following signals can be shown:

.. list-table:: Nova LED States
   :widths: 25 25
   :header-rows: 1

   * - LED State
     - Meaning
   * - Solid Blue
     - The device is switched on, and is actively connected through Bluetooth. This happens when SenseCom is running.
   * - Blinking Blue
     - The device is switched on, and is not (yet) connected through Bluetooth.
   * - Red
     - The battery level of your device is low. Charging is recommended.
   * - Blinking Green
     - The Nova Glove is turned off, and the battery is currently being charged.
   * - Solid Green
     - The Nova Glove is turned off, and the battery is fully charged.

.. note::
   When the Nova Glove is turned on, its LED will not show that it is charging. You can use SenseCom to view its charging state and battery level.



Detachable Soft Glove
~~~~~~~~~~~~~~~~~~~~~

The Nova's Soft Glove and Hub can be separated from one another, allowing one to clean the soft glove by hand washing.
The Soft Glove comes in three sizes, and a set of Nova Gloves comes with one set of soft gloves, of a size of your preference.
It is possible to purchase additional sizes of Soft Gloves via your SenseGlove reseller.

Consult the chart below to see which Soft Glove size best suits your wearer(s):

.. image:: images/hand-size-guide.png

.. list-table:: Nova Soft Glove Sizes
   :widths: 25 25 25
   :header-rows: 1

   * - Size
     - Hand Width [mm]
     - Hand Length [mm]
   * - Small (S)
     - < 82
     - < 182
   * - Medium (M)
     - 82 - 88
     - 182 - 192
   * - Large (L)
     - > 88
     - > 192


.. tip:: Can’t choose between two sizes? The slightly smaller size will usually result in the best product performance.




Compatibility
-------------

The Nova Glove is compatible with devices that allow for a Bluetooth Connection, and for which SenseGlove has an API available.
For a list of up-do-date compatibility of the Nova Glove, please check out our :doc:`Compatibility Page <compatibility>`.


Further Guides
--------------


What's in the Box?
~~~~~~~~~~~~~~~~~~

.. youtube:: Nv_4S0KUb0U

Video going over the contents of a SenseGlove Nova Kit.


Nova Manual
~~~~~~~~~~~

The Nova manual, which contains further information, can be :download:`downloaded here <pdfs/Nova_Manual_V0_9.pdf>`.


Attaching Tracking Mounts
~~~~~~~~~~~~~~~~~~~~~~~~~

.. youtube:: XLA2xKVLxWU

Video guide on attaching Vive Trackers.



.. youtube:: ZCks06_Vwko

Video guide on attaching Quest 2 Controllers.


Donning and Doffing
~~~~~~~~~~~~~~~~~~~


.. youtube:: U_giuElkW7U

Video guide on how to equip your SenseGlove Nova.
