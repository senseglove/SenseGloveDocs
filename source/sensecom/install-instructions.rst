Installation Instructions
=========================
This section explains how to install the :doc:`SenseCom <overview>` software on your system.
You'll need it to communicate with your SenseGlove Devices.

In most cases, the latest SenseCom binaries will be pre-packaged with the SenseGlove API of your choice. You can always find the latest version of SenseCom on (`its github page <https://github.com/Adjuvo/SenseCom>`_).

 
Latest Binaries
---------------

- Latest Windows Release (`installer <https://github.com/Adjuvo/SenseCom/raw/main/Win/SenseCom_install_1_3_0.exe>`_)
- Latest Linux Release (.zip)


Installing via GitHub
---------------------

.. tabs::

   .. group-tab:: Windows

       - Clone the github repository (https://github.com/Adjuvo/SenseGlove-API) or download the binaries as a .zip file. 
       - Run the SenseCom_install_X_X_X.exe inside the Win/ folder.
       - Go through the installation instructions.
       - As part of the installation, you will download a Visual C++ Package. If you are developing for C++, you likely already have this package installed. This installlation can 'fail' if you already have said package installed. In that case, dismiss the message, and continue your installation.

       Alternative Method:

       - Clone the github repository (https://github.com/Adjuvo/SenseGlove-API) or download the binaries as a .zip file. 
       - You will need a Visual C++ package to be able to use the library, which you can download or find in the SenseCom/Win folder. If you're developing with C++, you'll likely have this installed already. You will need a Visual C++ package to be able to use the library, which you can download or find in the SenseCom/Win folder. If you're developing with C++, you'll likely have this installed already.
       - Run SenseCom.exe in the SenseCom/Win folder.

   .. group-tab:: Linux

       - Clone the github repository (https://github.com/Adjuvo/SenseGlove-API) or download the binaries as a .zip file. 
       - Right click the SenseCom.x86_64 in the SenseCom/Linux folder go to the permissions tab, and select "allow running as executable".
       - Ensure that you have set the permissions for SenseCom to use your serial ports via the "sudo adduser $USER dialout" command.
       - Run SenseCom.x86_64 SenseCom/Linux

       Alternatively, you can follow along with the video below:

       .. youtube:: f34ofFkx_Ow

   .. group-tab:: Android

       - Installing SenseCom is not required for the Android Platform, as the process runs internally for each app.

