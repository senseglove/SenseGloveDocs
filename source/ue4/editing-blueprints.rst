Editing blueprint default settings
==================================

.. note::
   This page is part of the Legacy Unreal Plugin, which is no longer actively maintained.
   We recommend checking out the actively maintained version :doc:`here<../ue5/ue5-index>`.


Developers can either use the default values of a blueprint or edit these values to customize the interaction logic.

SGController Blueprint
----------------------

In SGController setting detail's pane on the right side of the window; you can find a **Sense Glove** section.

.. toctree::
   :maxdepth: 2

.. image:: images/08_SGController_Setting.PNG

-  **Glove Type** (Left Hand or Right Hand): Choose your preferred hand value from the drop-down
-  **Default Grabbing**: To deactivate un-check the checkbox 
-  **Default Physics**: To deactivate un-check the checkbox 
-  **Enable Debug**: To deactivate un-check the checkbox


Animation
^^^^^^^^^

Animation section is used to get data from the Sense Glove into Unreal. 
**SenseGloveAnimInstance** in the **Anim Class** drop-down is enabled by default.

.. toctree::
   :maxdepth: 2

.. image:: images/16_MeshContainer_Animation_Dropdown.PNG


Mesh
^^^^

Mesh section in the Mesh Container-**Details** section helps you to pick you the hand models.

.. image:: images/10_MeshContainer_Setting.PNG

.. important:: Chosen hand models should be similar to the existing hand model.


Grab Areas and Collision Capsules
---------------------------------

Grab Area (Thumb and Index)
^^^^^^^^^^^^^^^^^^^^^^^^^^^
Grab areas are applicable only for Thumb and Index fingers. Grab areas can be adjusted to make it easier (or more precise!) to grab objects.

**Shape**

To adjust the box area, enter the values in the X,Y and Z axis of **Box Extent** fields.
To adjust the line width, enter the values in the **Line Thickness** field.

.. image:: images/14_IndexGrabArea_Shape_Menu.PNG

.. image:: images/grabarea1.png


Capsule (Thumb, Index, Middle and Ring)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Capsules are used for physics interactions. It's applicable for Thumb, Index, Middle and Ring fingers.

.. image:: images/capsule1.png

**Shape**

To adjust the half height size, enter the value in the **Capsule Half Height** field.
To adjust the thickness of the capsule enter the value in the **Capsule Radius** field.

.. image:: images/13.1_Thumb_Capsule_Shape_Menu.PNG


**SenseGlove Collider**

SenseGlove Collider settings can be customised to adjust the impact feel when you touch an object.
Choose the preferred time value in the **Static Hit Duration** field to provide the impact feel.
Enter the preferred values in the **Hit Intensity** field to set the hit impact feel.

.. image:: images/12_Thumb_Capsule_Collider_Menu.PNG


