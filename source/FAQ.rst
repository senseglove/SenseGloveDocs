Frequently Asked Questions
==========================

Is the SenseGlove API compatible with (Insert HMD here)?
--------------------------------------------------------
If you can interface with your HMD using a programming language or game engine that the SenseGlove API is also compatible with,
you'll be able to develop XR applications using any SenseGlove hardware. However, you do need some way to track the hands in 3D space,
which is what complicates matters. You can check the latest info on our :doc:`Compatibility page <compatibility>`.


Is the SenseGlove Nova Compatible with any VR games?
----------------------------------------------------

Unfortunately not. An integration with OpenVR / OpenXR would allow you to move your hands within VR games that support hand tracking through these standards,
but the current generation of SenseGlove devices have no buttons that can be easily mapped to controls within those games. 
Furthermore, most VR games have no way to determine force-feedback unless they are built with either the SenseGlove Plugin(s) or with Interhaptics. 


My Nova Glove is plugged into the PC via USB, but it is not recognized by SenseCom
----------------------------------------------------------------------------------

The SenseGlove Nova is not designed to send data through its USB port. Its USB port is meant for charging the device and the occasional firmware update only.
You will need to ensure your Nova is turned on, and :doc:`paired to your device <connecting-devices>`.


Do I need to run SenseCom on my Oculus Quest / Pico Neo to be able to connect to my Nova Gloves?
------------------------------------------------------------------------------------------------

You do not need to run SenseCom on your standalone device.
When building for Android using the SenseGlove Unity Plugin, a different communications library is used. As opposed to the Desktop builds, this version runs within the software itself. 
﻿We do offer an Android version of SenseCom that runs on Quest 2, which you can use to test and troubleshoot your devices.
You will need to :doc:`pair your SenseGlove Nova <connecting-devices>` to your standalone Android device for it to show up in your simulation.

Please note that if SenseCom is running on your desktop at the same time as your Android app, you might experience connection issues.
Both devices will connect at the same time, causing conflicts. We recommend shutting down SenseCom on your desktop when testing on Android.

