Interhaptics
============

An Integration with the Unity version of the `Interhaptics <https://www.interhaptics.com/>`_ Interaction Builder and Haptic Composer is also available for the SenseGlove DK1. It is not (yet) available for the Nova Gloves.

This integration is mostly developed and maintained by Interhaptics. We refer you to `their documentation <https://www.interhaptics.com/resources/docs>`_ for support and guidance.