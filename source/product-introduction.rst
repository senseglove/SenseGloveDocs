Product Introduction
====================
SenseGlove allows you to feel shapes, textures, stiffness, impacts and resistance of any virtual object, 
so you can experience digital worlds, through intuitive real-world behaviour.

SenseGlove's patented Force-Feedback system works by putting a variable brake force up to 20N on cables that run along the hand.
This prevents the user from grasping through virtual objects, which is the most crucial element of realistic simulation, 
providing the feeling of the size and density of virtual objects. 


Nova 1
--------
An evolution of the SenseGlove Dk1, built specifically with usability in mind:
It is fully wireless, compatible with standalone headsets and made to be donned within 5 seconds.


The SenseGlove Nova features Force-Feedback on all fingers except the pinky finger, and vibrotactile actuators on the thumb, index finger and wrist.
An in-depth guide to your SenseGlove Nova is :doc:`available here <nova-glove>`.


.. youtube:: TajCGQJTrT8



Nova 2
--------

The Nova 2 is the next evolution of the Nova 1. It features an additional sensor, active contact feedback of the palm, and overall usability improvements.

The SenseGlove Nova features Force-Feedback on all fingers except the pinky finger, and vibrotactile actuators on the thumb, index finger and hand palm.
An in-depth guide to the Nova 2.0 is :doc:`available here <nova-2>`.


.. youtube:: SZGcC79Ed


SenseGlove DK1
----------------
The original SenseGlove product, this Exoskeleton boasts force- and vibrotactile feedback actuators on each of the fingers,
as well as a total of 20 sensors integrated into the mechanical links. Built into the hub are a larger vibration motor and rotation sensor.


An in-depth guide to your SenseGlove DK1 is :doc:`available here <dk1-glove>`.


.. youtube:: jw7Qtoff9GM


.. note:: The SenseGlove DK1 is out of production as of Q1 2022. The DK1 is currently no longer being supported. 


Wireless Kit
~~~~~~~~~~~~
Want your DK1 to be wireless like the Nova? You can, with the SenseGlove Wireless Kit:

.. youtube:: hK55AzQgNp4


For more interactive videos, subscribe to our youbtube channel: https://www.youtube.com/channel/UCWWa-1FWN5WMKgqtrGj0qbQ
