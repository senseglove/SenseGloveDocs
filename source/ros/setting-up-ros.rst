Setting up a workspace
========================
- Make sure you are on a Ubuntu 18.04 system (not necessary, but all code has been checked with docker images running in ubuntu).
- Download ros-melodic as described in :doc:`ros-first-timers`.
- Clone our workspace by either using git clone https://github.com/Adjuvo/senseglove_ros_ws.git or through the webbrowser here on the Github website.
- Install the following dependencies:

  .. code-block:: bash

        sudo apt-get install ros-melodic-ros-control
        sudo apt-get install ros-melodic-joint-trajectory-controller
        sudo apt-get update
        rosdep update
        sudo apt-get upgrade

- Navigate, in the terminal, to the workspace folder
- Source your workspace: ``source /opt/ros/melodic/setup.bash``
- Build your workspace: ``catkin build`` or if you prefer a different build tool use: ``catkin_make`` or use: ``colcon build``
- After building, you can source the workspace itself by using ``source devel/setup.bash``
