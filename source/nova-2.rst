SenseGlove Nova 2
===================

An evolution of the :doc:`Nova 2.0 <nova-glove>` - designed to improve tracking fidelity, ease of use, and to extend its haptic capabilities. 


.. youtube:: SZGcC79Ed-M


The Nova 2.0 was released in Q2 2024, and is currently available through the `SenseGlove webshop <https://www.senseglove.com/nova-2/>`_.



Product Features
----------------

The device consists of two separate parts; the "Hub" and "Soft Glove", which can be separated.
The Hub contains all sensors and actuators, while the Soft Glove is the wearable part that ensures a good fit to the body.


Force-Feedback
~~~~~~~~~~~~~~

The Nova 2.0 provides 1 Degree of Freedom of Force-Feedback on the thumb, index finger, middle finger and ring finger. The glove does not provide force-feedback on the pinky finger.
The system works by applying a brake force to the cables that run along the back of the hand, which inhibits the flexion movement.
You are always able to extend your fingers, but you won't be able to flex them past a certain point.
The Nova's Force-Feedback actuators do not pull on their cables; meaning that they cannot move the fingers for you, nor can the glove move by itself.

The Nova 2.0 can provide up to 20N of force along each finger that is equipped with a force-feedback motor. 
From an API level, one can set the force on each of these fingers with a single command, at a rate of up to 200Hz and a resolution of 100 steps.


Active Contact Feedback
~~~~~~~~~~~~~~~~~~~~~~~

The front strap of the Nova 2.0 can tighten itself by up to 5 mm with a force of up to 20 N, creating a pressure on the palm of the hand, akin to someone holding your hand.

From an API level, you can set the level of 'pressure' the strap exerts on the user, from 0 - 100%, at a rate of 200Hz.

.. note:: This active feedback does not use no force-sensors; it uses a position sensor.


Vibrotactile Feedback
~~~~~~~~~~~~~~~~~~~~~

The Nova 2.0 has four (4) Linear Resonant Actuators (LRAs): 
Two vibration motors are located inside the front strap to play vibrations on the hand palm; one on the index-finger side, and one on the pinky-finger side.
The other two vibration motors are located on the tip of the thumb and index finger. These last two vibration motors are connected to the Hub via conductive yarn inside the soft glove.

From an API level, one can send configurable waveforms to each individual actuator. These wavefroms can vary in (among other things) amplitude, duration and frequency, and can be sent at a rate of up to 100Hz per actuator.

The Nova 2.0's LRA Resonant Frequency is around 170Hz.

Finger Tracking
~~~~~~~~~~~~~~~

The Nova 2.0 measures the flexion / extension of the thumb, middle finger and ringer finger with a single sensor each, and has two sensors to measure index finger flexion. 
In addition, it has a sensor to measure thumb abduction / adduction. The pinky finger flexion is linked to that of the ring finger. These movements are captured by measuring the extension of the cables on the Nova 2.0. 

The Nova 2.0 measures cable extensions at a rate of 60 Hz, with a resolution of 0.03 mm. However, your calibration will ultimately determine the resolution of the hand pose calculated from this data.

The sensors used to measure this extension are not absolute: When restarting the glove, the sensor readings might differ. The cable extensions will also differ between users. It is therefore required to recalibrate the Nova 2.0 at the beginning of each session. This calibration runs on the glove. The SenseCom software offers a visual guide to accompany calibration, and our Unity Plugin has components to add a similar guide into your VR system.



Wrist Tracking
~~~~~~~~~~~~~~

The Nova 2.0 does not have built in hand-tracking: It cannot determine its position in 3D space by itself, and instead relies on mouting a 3rd party device, such as a Vive Tracker, onto the glove.
Computer Vision models are being developed to track the Nova 2.0(s) in 3D space using camera systems, but these are still under heavy development.


.. image:: images/nova-trackermounts.png


.. note:: 
    Integrating the glove directly into a 3rd party system, such as the `Vive Lighthouse system <https://www.roadtovr.com/tundra-labs-steamvr-tracking-hdk-tl448k6d-gp-hdk/>`_, would increase the size of the device, and limits it to that system only. Developing a tracking system built into the glove is not possible with the Nova 2.0's current hardware.

The Nova 2.0 does contain an Inertial Measurement Unit (IMU). It uses a Magnetometer, Accelerometer and Gyroscope to measure the glove's rotation relative to the earth's magnetic field.
You can combine the IMU with a 3rd party tracking device to include lower arm tracking into your simulation, though note that an additional calibration step will be required to map the movements of the IMU to the wrist.

.. note::
   The IMU takes a few seconds to calibrate - at which point, it might 'snap' to a new rotation. This only happens once, usually in the first few seconds of turning on the glove. 


Bluetooth
~~~~~~~~~

The Nova Gloves uses the Bluetooth 4.2 protocol. Specifically the Bluetooth 4.2 BR/EDR, with SPP (serial port profile).



Compatibility
-------------

The Nova 2.0 is compatible with devices that allow for a Bluetooth Connection, and for which SenseGlove has an API available.
For a list of up-do-date compatibility of the Nova 2.0, please check out our :doc:`Compatibility Page <compatibility>`.


Updating your Nova 2.0 Firmware
-------------------------------

It is possible to update your Nova 2 Glove's firmware over Bluetooth. 
To update, you will need the SenseCom Software version v1.7.1 or higher, running on a Windows PC with an Internet and Bluetooth Connection.

.. note::
   All Nova 2 Gloves running firmware version v1.0 are able to use this 'over-the-air' update method.
   However, depending on the age of your device, you might need multiple firmware updates to get to the latest version due to changes in firmware compression and/or communication protocols.


When is a firmware update required?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is not mandatory to update your device's firmware.
However, to leverage on new functions and/or bugfixes, we recommend updating it when a new update becomes available.


Update SenseCom to version v1.7.1 or higher
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You can find installer(s) for your relevant plaform on the `SenseCom GitHub page <https://github.com/Adjuvo/SenseCom>`_.
You'll need to install the latest version to pull these firmware updates.

Once you've installed the update and :doc:`connect to your Nova 2 Glove <connecting-devices>` and run the SenseCom Software.


Detecting if a firmware update is required
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When a glove connects to SenseCom, we check its firmware version against the latest one(s) hosted on `The Nova 2 GitHub page <https://github.com/Adjuvo/nova2-firmware>`_.
If a firmware update is available, a small (!) icon will appear next to your glove. As one might expect, an internet connection is required to pull the latest firmware from this online repository.

.. image:: images/n2fw-hoverIcon.png

When hovering over said glove icon, you'll see message showing you the next step: Opening the "Device Details" Menu.

.. note::
   If no icon appears, your glove firmware is up to date as far as SenseCom can tell.



Starting the Firmware Upload
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Right click on your glove icon, or click on the device name below the icon, to bring up a menu with several options. Press "Details" to go the "Device Details" Page.
If your glove requires a firmware update, an "Update Firmware" button will appear near the bottom of the UI.



.. image:: images/n2fw-updateBtn.png


Clicking this button will prepare your glove for a firmware update. You will be prompted with instructions and an "Upload" Button you can press.


.. image:: images/n2fw-startUpload.png

.. note::
   To succesfully upload the firmware, please ensure that your glove stays on during the process. The best practice is to connect your glove to its charger, so there is no risk of the device turning off.


After pressing the Upload Button, you will be presented with a progress bar, letting you know how much of the firmware has been uploaded.
There is also a timer below to give you a **rough estimation** of how long the process will still take.


.. image:: images/n2fw-progress.png


Once your bar reaches 100%, it's time to finalize the update.


.. tip::
   Since the new firmware is being uploaded via Bluetooth, the process will be much faster when no other Bluetooth Device(s) are connected.


Finalizing the Update
~~~~~~~~~~~~~~~~~~~~~

After receiving the new update in full, your Nova Glove will reboot. It will take a couple of seconds for the glove to re-initialize after rebooing from a firmware update.


.. image:: images/n2fw-reinitialize.png


Once this process is complete, and the new firmware version is registered in the SenseGlove Back-End, you will return back to the main SenseCom screen.
Congratulations! Your Nova Glove has now been updated to the latest firmware version!
To verify this, you can once again open the "Device Details"  menu to see your device's new firmware version.


.. image:: images/n2fw-finished.png



The glove still needs a firmware update
#######################################

If you exited out of the Firmware Upload process before the glove has finished rebooting, there is a chance the new firmware version is not yet registered in the back-end.
In that case, rebooting SenseCom should clear the issue.


On rare cases, you may need to update to one firmware version before being allowed to update to the latest one. When in doubt, restarting SenseCom and rebooting your glove are always the best way to verify this.


Oh no! Something went wrong!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If your glove turned off during any point in the firmware update, you can return back to the main menu, and reboot your glove.
You will be notified of any odd errors that occur during the process.
If any communication issues occur during the firmware update process, the best practice is to reboot your glove before trying again. 


.. image:: images/n2fw-error.png


If, after receiving a firmware update, your glove is no longer being recognized as a SenseGlove Device, even after using the "Retry Connections" button several times, have no fear: It is possible to return a glove back to factory settings through a separate process. Please contact support@senseglove.com with your glove's serial number. If possible, please close your SenseCom and attach a sessionlog.txt that is generated in MyDocuments/SenseGlove/.


I want to upload a specific firmware version
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This functonality is not (yet) available though SenseCom. 
If it is absolutely imperative you update to a specific firmware version, please contact support@senseglove.com for information.


Can I sign up for 'beta' firmware releases?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This functonality is not (yet) available though SenseCom.