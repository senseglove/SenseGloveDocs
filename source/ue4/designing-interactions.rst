Designing interactions
======================

.. note::
   This page is part of the Legacy Unreal Plugin, which is no longer actively maintained.
   We recommend checking out the actively maintained version :doc:`here<../ue5/ue5-index>`.

It possible to add additional collision boxes and design custom interactions.

WIP

Custom interactions
~~~~~~~~~~~~~~~~~~~


.. image:: ./images/custom-interaction.gif
   :width: 500


