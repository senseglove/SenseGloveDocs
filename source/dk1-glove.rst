SenseGlove DK1
================

The original SenseGlove product; this exoskeleton boasts force- and vibrotactile feedback actuators on each of the fingers, as well as a total of 20 sensors integrated into the mechanical links. 
Built into the hub are a larger vibration motor and an Inertial Measurement unit (IMU).

.. youtube:: jw7Qtoff9GM

.. note:: The SenseGlove DK1.X is out of production as of Q1 2022. However, any software updates made from then on will still be compatible with the DK1.X Hardware, until al least Q1 2024.



Product Features
----------------

The SenseGlove DK1 consists of five (5) identical “fingers” mounted on a central “hub”.


Force-Feedback
~~~~~~~~~~~~~~

The SenseGlove DK1 provides 1 Degree of Freedom of Force-Feedback on the thumb, index finger, middle finger, ring finger and pinky finger.
The system works by applying a brake force to the cables that through the exoskeleton linkages, which inhibits the flexion movement.
You are always able to extend your fingers, but you won't be able to flex them past a certain point.
The DK1's Force-Feedback actuators do not pull on their cables; meaning that they cannot move the fingers for you, nor can the glove move by itself.

The DK1 Glove can provide up to 20N of force along each finger that is equipped with a force-feedback motor. 
From an API level, one can set the force on each of these fingers with a single command, at a rate of up to 200Hz and a resolution of 100 steps.


Vibrotactile Feedback
~~~~~~~~~~~~~~~~~~~~~

The SenseGlove DK1 has one Eccentric Rotating Mass (ERM) vibrotactile actuator integrated into each finger, located in the black linkage just before the thimbles.
From an API level, one can directly set the amplitude of each of these vibration motors with a single command, at a rate of up to 200Hz and a resolution of 100 steps.

The amplitude you set is used to determine the voltage (0 … 3.3V) that drives the actuator. 
These are Vibronics ERM actuators to create vibrotactile feedback, for which you can find the datasheet `here <https://www.vybronics.com/coin-vibration-motors/with-brushes/v-c1026b002f>`_. 
Unfortunatley, the manufacturer does not provide any ‘driving voltage to vibration force’ relation. We only know that at 3.3V, the actuator should vibrate with its maximum intensity of 0.8G.



Finger Tracking
~~~~~~~~~~~~~~~

Each finger of the SenseGlove DK1 measures four (4) angles between linkages, 20 in total, to create an accurate representation of the glove in 3D space.
It measures these at a rate of 120Hz, with a resolution of 0.35° (degrees).
A full reference of the SenseGlove DK1 Exoskeleton kinematics can be found :doc:`here <kinematics/dk1-kinematics>`.

The sensors used to measure these angles are absolute. In between power cycles, it is not required to recalibrate the SenseGlove DK1.



Wrist Tracking
~~~~~~~~~~~~~~

The SenseGlove DK1 does not have built in hand-tracking: It cannot determine its position in 3D space by itself, and instead relies on mounting a 3rd party device, such as a Vive Tracker, onto the glove.


.. image:: images/dk1-trackermounts.png


The SenseGlove DK1 does contain an Inertial Measurement Unit (IMU). It uses a Magnetometer, Accelerometer and Gyroscope to measure the glove's rotation relative to the earth's magnetic field.
You can combine the IMU with a 3rd party tracking device to include lower arm tracking into your simulation, though note that an additional calibration step will be required to map the movements of the IMU to the wrist.

.. note::
   The IMU takes a few seconds to calibrate - at which point, it might 'snap' to a new rotation. This only happens once, usually in the first few seconds of turning on the glove. 



Compatibility
-------------

For a list of up-do-date compatibility of the SenseGlove DK1, please check out our :doc:`Compatibility Page <compatibility>`.





Wireless Kit
------------
Want your DK1 to be wireless like the Nova? You can, with the SenseGlove Wireless Kit:

.. youtube:: hK55AzQgNp4

.. note:: Together with the SenseGlove DK1.X, the Wireless kit is out of production as of Q1 2022. However, any software updates made from then on will still be compatible with the DK1.X Hardware, until al least Q1 2024.


Further Guides
--------------


SenseGlove DK1 Manual
~~~~~~~~~~~~~~~~~~~~~

The Nova manual, which contains further information, can be :download:`downloaded here <pdfs/dk1-Manual-1-3v1.pdf>`.



Wearing the DK1
~~~~~~~~~~~~~~~

.. youtube:: 2NJV_pKUiYE

Video Guide on how to don and doff the DK1 Glove.


Connecting the DK1 to a computer
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. youtube:: Q__OIfjJLIc

Video Guide showing how to connect the DK1 to a computer.


Attaching HTC Vive Controllers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. youtube:: HfstxdKKxC4


Video guide how to attach HTC Vive Trackers to the DK1. 


Attaching Oculus Rift Controllers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. youtube:: gxDfSrCCdkU

Video guide how to connect Rift S / Touch controllers.


