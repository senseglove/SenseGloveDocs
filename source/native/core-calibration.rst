SGCore Calibration Functions
============================



This page details calibration functions for both the Nova 1.0 and Nova 2.0. These are not relevant for the SenseGlove DK 1.0, the blue exoskeleton.


For C++ Back-end v2.0, changes have been made to Nova Glove calibration: It now happens automatically in the back-end of the API and, with the Nova 2.0, on the glove itself! This means that no external calibration algorithm is required (though you could still write one yourself if desired).


Why do we need calibration?
---------------------------

The Nova's sensors measure cable extension, but they are not absolute encoded. In addition, every hand size has a different cable extensions that correspond to stretched and flexed fingers. As such, we need to "Normalize" this sensor data: mapping raw ADC values to values between 0 ... 1, each time a user (re) equips the glove.

For example, the index finger flexion for you may may be mapped in ADC values to the range [500-900], while someone with a smaller hand may move in the [200, 700] range. Normalizing data means both users' sensor data will be output as [0.0 .. 1.0].

This Normalized data offers a slightly more usable format to calculate joint angles, and is used for our finger tracking algorithms.


Calibration Basics
------------------

When you first start up your simulation, you may notice that your virtual hand does not move until you move your real hand by a small amount. Each sensor on the glove is calibrated individually: As soon as one of them moves a minimum amount (roughly 30% flexion, for example), you'll see the finger joint(s) begin to move.

The user is required to move their fingers until all sensors have moved a minimum amount. This can be done in a guided 'calibration void' with instructions, like in our Unity Engine, or simply happen throughout use.

When you're satisfied with your calibration, a thumb up gesture locks it in place, and a haptic signal is played.


All in all, the gloves go through the following calibration state:

.. code-block:: c++

   /// <summary> The Calibration State of this Glove. </summary>
   enum class SGCORE_API EHapticGloveCalibrationState : int8_t
   {
       /// <summary> No idea what the calibrationState of this glove is... </summary>
       Unknown,
       /// <summary> One or more sensors still need to move... </summary>
       MoveFingers,
       /// <summary> All sensors have moved. </summary>
       AllSensorsMoved,
       /// <summary> Calibration is locked in. Done collecting Data. </summary>
       CalibrationLocked
   };



Once calibration is locked in, you're free to take off and don the gloves again. However, at the moment, this calibration is not yet saved in between sessions!
API functions to save / load calibration profiles will become available in the future.


Calibration API Functions
-------------------------

Sensor calibration happens automatically whenever you call a function to retrieve hand poses from a Nova Glove. That being the following functions:


.. tabs::

   .. group-tab:: HandLayer

      .. code-block:: c++

         /// <summary> Attempt to get the HandPose through any of our gloves. You don't care which one is on the other side, as long as you get one. </summary>
         static bool GetHandPose(bool rightHand, HandPose& out_handPose);

   .. group-tab:: Device-Specific API

      .. code-block:: c++

         // Contained in HapticGlove.hpp, NovaGlove.hpp, Nova2Glove.hpp

         /// <summary> Returns the Hand Angles calculated by this Nova Glove. Used for input for HandPoses, but can be used in and of itself. </summary>
         virtual bool GetHandAngles(std::vector<std::vector<Kinematics::Vect3D>>& out_handAngles) const override;

         /// <summary> Retrieve a HandPose based on the latest profile and default hand Geometry. </summary>
         bool HapticGlove::GetHandPose(HandPose& out_handPose) const

         /// <summary> Calculate a HandPose from this glove's latest Sensor Data </summary>
         virtual bool GetHandPose(const Kinematics::BasicHandModel& handGeometry, HandPose& out_handPose) const override;


         // NovaGlove.hpp, Nova2Glove.hpp only

         /// <summary> Returns the normalized Sensor Data of this Nova Glove. If no normalization is running on the glove(s), then it's done on the PC side. </summary>
         bool GetNormalizedInput(std::vector<float>& out_normalizedValues) const;




Note that sensor normalization occurs when you're collecting the Nova Glove's sensor data. It is assumed that one has their own means of calibration for this glove.


Calibration State and Controls
------------------------------

You can always poll the Nova Glove's Calibration Status; which will either be stored internally, or be part of the device's sensor data. Alternatively, you can retrieve an instruction string that will tell your user exactly what still needs to happen. You can also Reset and End calibration manually through the HandLayer or


.. tabs::

   .. group-tab:: HandLayer

      .. code-block:: c++

         /// <summary> Retruns the calibration state of the chosen hand. </summary>
         static EHapticGloveCalibrationState GetCalibrationState(bool rightHand);

         /// <summary> Returns the Calibration Instructions for the chosen hand </summary>
         static std::string GetCalibrationInstructions(bool rightHand);

         /// <summary> Reset the calibration of the left/right hand. </summary>
         static void ResetCalibration(bool rightHand);

         /// <summary> Reset the calibration of the left/right hand. </summary>
         static void EndCalibration(bool rightHand);

   .. group-tab:: Device-Specific API

      .. code-block:: c++

         // Contained in HapticGlove.hpp, NovaGlove.hpp, Nova2Glove.hpp

         /// <summary> Retruns the calibration state of the chosen hand. </summary>
         EHapticGloveCalibrationState GetCalibrationState();

         /// <summary> Returns the Calibration Instructions for the chosen hand </summary>
         std::string GetCalibrationInstructions();

         /// <summary> Reset the calibration of the left/right hand. </summary>
         void ResetCalibration();

         /// <summary> Reset the calibration of the left/right hand. </summary>
         void EndCalibration();


