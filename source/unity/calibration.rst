Unity Hand Calibration
======================

Last updated on 17/04/2024 for Unity Plugin v2.6.1.

Your Nova Glove needs to know which cable extensions correspond to your finger movements - which is different for each user and, due to the type of sensor used, also changes with each power cyle.


By default, calibration happens automatically in the background, via the SenseGlove API. There is no need to initialize it anymore.
To avoid any grotesque movements while the glove is uncalibrated, you'll need to move each sensor a little bit before the hand begins to animate.
Once each finger joint is moving, a 'thumbs up' gesture will lock your calibration for the session, though this step is optional.
You may want to reset calibration in between users.


Calibration via SenseCom
------------------------

If you have :doc:`SenseCom<../sensecom/overview>` installed on your PC, you can run Calibration via this program instead.


When SenseCom discovers a new connection to a Nova Glove, a 'Calibrate' Button appears on the top menu. Pressing this button will allow you to (re)calibrate your Nova Glove.
If you have 'Automatic Calibration' enabled in SenseCom's Settings Menu, it will start the Calibration process once your first glove connects. 

.. image:: ../sensecom/images/SC_calibration.png 

This process runs in the background, and can be completed without ever looking to SenseCom. However, a new profile is only loaded when the focus returns to your Unity Applications.
Alternatively, you can force a reload of the calibration profiles by calling :code:`SG_HandProfiles.TryLoadFromDisk();` on the `SG_HandProfiles <https://github.com/Adjuvo/SenseGlove-Unity/blob/master/SenseGlove/Scripts/Calibration/SG_HandProfiles.cs>`_ script when you deem it necessary. 


The calibration done via SenseCom will stay valid until you close the program. When you restart SenseCom, you will need to recalibrate.


.. note::
   This step is only useful when your demo is running on Windows or Linux. On Android, we cannot easily access files from a shared directory without extensive permissions.
   As such, Calibration on Android is only stored inside the App's folder.


Calibration on Android via 'Calibration Void'
---------------------------------------------

The 'Calibration Void' is a standard scene included in your Unity Plugin, located in SenseGlove/Calibration.
It comes in two variations; one for 2D applications ( CalibrationVoid ) and one for XR applications ( CalibrationVoid_XR ) .
It uses a logic script, `SG_CalibrationVoid <https://github.com/Adjuvo/SenseGlove-Unity/blob/master/SenseGlove/Scripts/Calibration/SG_CalibrationVoid.cs>`_, to start and end calibration through the :code:`SG_CalibrationSequence`s described above.

.. image:: images/calibrationVoid.png

This scene wil 'lock' your user into a dedicated void with instructions that follow their gaze, and example hands that demonstrate a series movements to make for calibration.
If their hands have moved enough, they can confirm calibration by giving a 'thumbs up' gesture, after which they can (automatically) pass on to the next scene.

Setting this as the 'Staring Scene' (Build Index 0) of your build will ensure your user is properly calibrated before the simulation can begin.


Automatic Transition
~~~~~~~~~~~~~~~~~~~~

Using the :code:`changeSceneAfter`, :code:`goToSceneIndex` and :code:`goToSceneName` parameters, you can have the :code:`SG_CalibrationVoid` script automatically transition to your main scene.
The Calibration Void will load this next scene in the background once the first glove is detected, so once calibration is complete, this transition can be instant.

* :code:`changeSceneAfter`: If this value is < 0, the CalibrationVoid will not proceed to any next scene.
* :code:`goToSceneName`: The name of the scene to load, via :code:`UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(string sceneName);`. If this value is an empty string, the algorithm will look to :code:`goToSceneIndex`: for the next scene.
* :code:`goToSceneIndex`: The Build Index of the next scene to load, via :code:`UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(int sceneBuildIndex);`. If this value is < 0, the CalibrationVoid will not proceed to any next scene.

The CalibrationVoid script has a CalibrationCompleted event, which fires once the algorithm completed. You can hook into this event to call your own scene transitions.



Resetting Calibration
---------------------

During a session, you may want to recalibrate the hand. For example, when you switch users, or when the glove is donned after the application has started.

In Code
~~~~~~~~

The easiest method to reset calibration is to call the static function

.. code-block:: c#

   SGCore.HandLayer.ResetCalibration(true); //Resets the right Hand profile
   SGCore.HandLayer.ResetCalibration(left); //Resets the left hand profile

If you have a reference to any SG_HandComponent or a SG_TrackedHand:

.. code-block:: c#

   SG_TrackedHand myHand;
   SGCore.HandLayer.ResetCalibration(myHand.TracksRightHand());


Returning to the CalibrationVoid
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you have included the Calibration Void inside your simulation, you can return your used to said scene to automatically reset calibration, and re-run the calibration sequence wiht instructions.