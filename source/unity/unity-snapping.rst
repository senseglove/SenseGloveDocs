Hand Snapping and Finger Animation Overrides
============================================

Sometimes, you want your hands to always 'snap' on a fixed location on your 3D Model; for instance, when working with Drills or other hand tools.
In addition, you might want to limit the finger motion to a predefined pose, or restrict it within a certain range. All of this can be achieved through the :code:`SG_SnapOptions` component.



.. note::
   This section is only relevant if you're using the SenseGlove Manipulation System, via the SG_Grabable Script.


The SG_SnapOptions Component
----------------------------

This component can be added to GameObject(s) that have an :code:`SG_Grabable` (or any script that inherit from it) attached.
The :code:`SG_Grabable` automatically checks for any :code:`SG_SnapOptions` on the game GameObject during its setup, though it's also possible to explicitly link it in the inspector.

When an :code:`SG_Grabable` is grabbed and an :code:`SG_SnapOptions` component is present, it will be used to determine the 'Relative Grab Location' of the hand relative to the object.
This results in the object moving in the position and rotation towards the hand's current location. The 3D hand model will stay in (roughly) the same place.
During the Update loop, the :code:`SG_SnapOptions` is used to control the hand's finger pose as well. Each finger can be fixed, limited, or free to move.


.. image:: images/snapOptions_handOverrides.png


The "Spawn Hand(s)" button on the :code:`SG_SnapOptions` script will spawn a copy of the 3D Model used for your SG_TrackedHand(s). If you have no SG_TrackedHands or SG_User's inside your scene, you'll receive a warning. When spawning a copy of the 3D Hand Model for the first time, this hand is places on a local position and rotation of [0.0f, 0.0f, 0.0f]. The goal is to position this 3D Model on your 3D object in the desired pose and location. Once your hand(s) are in the correct location and pose, the "Store Pose(s)" will save them inside that specific :code:`SG_SnapOptions` script, to be retrieved during runtime.



.. note::
   The Hand Pose(s) you set are stored as a serialized string, inside a public variable that is hidden in the inspector.
   We've noticed that, when using Unity Prefabs, this public string is reset during runtime (back to its default value of ""). Therefore, when you adjust the visual Hand Pose(s) on a Prefab object, please ensure you apply the changes made to the :code:`SG_SnapOptions` script before running your simulation!


Once you've stored your pose(s) inside the :code:`SG_SnapOptions` script, you also have a few buttons available. Below is an overview of all these buttons, and what they are meant to do:


- **Spawn Hand(s)** - Spawns a copy of the 3D Model(s) used by the SG_TrackedHand(s) inside the Scene.
- **Store Pose(s)** - Stores the current pose and location of the hands relative to the Grabable. Also removes the copies of the 3D Model(s).
- **Reset Left Pose** - Resets the pose of the left hand.
- **Mirror Fingers: Right -> Left** - Copies the finger positions of the right hand, mirrors them, and applies them to the left hand.
- **Reset Right Pose** - Resets the pose of the right hand.
- **Mirror Fingers: Left -> Right** - Copies the finger positions of the left hand, mirrors them, and applies them to the right hand.


.. note::
   The 3D Models that are spawned by the "Spawn Hand(s)" button will spawn as a child to your Grabable Object, and will be named "[Grabable Object Name]-POSER-LEFT" and "[Grabable Object Name]-POSER-RIGHT".




Snapping to a set location
--------------------------

By adding an :code:`SG_SnapOptions` component you your :code:`SG_Grabable`, we automatically assume you want to snap the hand to a specific location.

The hand location is stored relative to the grabable, so it is safe to move the Grabable Object around after clicking on the "Store Pose(s)" button.


.. note::
   The offset between 3D Hand Model and Grabable is calculated based on the Transform of the POSER-LEFT and POSER-RIGHT objects, and on the Grabable's :code:`baseTransform`. So make sure to move the enitre "POSER" GameObject (and not just the 3D Mesh) to the correct location. 
   
   When you change your Grabable's :code:`baseTransform`, your relative offsets will need to be updated. In that case, it's easiest to spawn your 3D Models, change your Grabable's :code:`baseTransform`, then click on "Store Pose(s)".





Finger Animation Overrides
--------------------------

You can use Unity's transform controls to manipulate the hand bones of the "POSER" models. When you hit the "Store Pose(s)" button, the positions and rotations are stored inside the :code:`SG_SnapOptions` as well. 


An :code:`SG_SnapOptions` component does not control finger movements unless the :code:`controlsFingerAnimation` is set to True. If you're only interested in locking the hand's grab location, you can leave the fingers in any pose you like, and leave this variable on false.


.. image:: images/snapOptions_fingerOverrides.png


With :code:`controlsFingerAnimation` set to true, five parameters will be visible; each controlling the "override more" of one finger.
The following Figner Tracking Override options are available:

- **None** - No finger contraints are applied; your finger will be allowed to move in any direction.
- **Limited** - The finger animation will be limited between a (simplified) upper and lower limit. You can set the :code:`previewLimits` variable to true to see the two extremes of movement that the finger will move between. 
- **Match Preview** - The finger will not be allowed to move, and be at a fixed location until you release the object.