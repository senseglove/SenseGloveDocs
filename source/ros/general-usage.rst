General Usage
=============

The ROS repository is meant to present a solid foundation for using the senseglove in ROS Melodic. As such it provides no concrete example projects. It does however provide the user with a few launch files, hardware.launch and senseglove_demo.launch which initiate the sensegloves. The senseglove_hardware_interface nodes which are called by these launch files do nothing more than using the senseglove API in a ROS /ros_control "sanctioned" manner.

.. important:: Run the sensecom application, present in the SenseGlove_API folder, when using hardware.launch! The launching of sensecom has been taken care of in senseglove_demo.launch in the bash script.

Users are advised to develop their own applications outside this package and make use of the provided topics. If users do find the need to write additions to this package, beware that the ROS repository is still subject to changes and pulling this repo again might override your own code.

Example: using two sensegloves in ROS
-------------------------------------
- Source your workspace
- Make sure your sensegloves are connected through usb or bluetooth if you checked your connection with sensecom, be sure to exit the application before proceeding
- Run: ``roslaunch senseglove_demo.launch`` A bash script is called invoking sensecom and running the hardware interface node twice for a left- and a right-handed glove. If all is well, your invocation of the roslaunch command should have started a roscore session and all necessary nodes providing intefaces to the senseglove. In a second (properly sourced) terminal you can verify that these nodes are publishing by invoking: rostopic list you can further test the application by checking that these topics get published by invoking: rostopic echo /topic_name

Example: using a single senseglove in ROS
-----------------------------------------

Though the whole infrastructure of this codebase was built upon the use with infinitely many sensgloves, our example launch file only accepts two gloves. Moreover, due to our integration into ros-control we require the user to know what type of gloves are connected to the PC. As such, the user has to define which glove is connected to the system.

- Find out if you are dealing with a left- or right-handed senseglove
- Remove the non-existing glove from the senseglove_hardware_demo.launch file, such that only your left/right-handed glove remains.
- Save the launch file
- Build you workspace
- Source your workspace
- Proceed as if you were dealing with 2 sensegloves