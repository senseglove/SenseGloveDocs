ROS First Timers
================

If you are totally unfamiliar with ROS we advise you to take a look at the ROS-wiki for a quick startup guide. We especially recommend the following tutorials:

- http://wiki.ros.org/melodic/Installation/Ubuntu
- http://wiki.ros.org/ROS/StartGuide
- http://wiki.ros.org/ROS/Tutorials
- http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment
- http://wiki.ros.org/ROS/Tutorials/WritingPublisherSubscriber%28c%2B%2B%29
